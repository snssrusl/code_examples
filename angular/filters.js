'use strict';

define(['angular', 'services'], function (angular, services) {

    /* Filters */

    angular.module('myApp.filters', ['myApp.services'])
        .filter('interpolate', ['version', function(version) {
            return function(text) {
                return String(text).replace(/\%VERSION\%/mg, version);
            };
        }])
        .filter('numsep',function(){
            return function(text){
                var text = text.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");
                return text;
            }
        })
        .filter('rawHtml', ['$sce', function($sce){
            return function(val) {
                return $sce.trustAsHtml(val);
            };
        }]);
});
