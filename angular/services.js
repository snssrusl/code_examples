'use strict';

define(['angular'], function (angular) {
    /* Services */

    // Demonstrate how to register services
    // In this case it is a simple value service.
    angular.module('myApp.services', ['angularFileUpload'])
        .value('version', '0.1')
        .value('testValue', 1)
        .constant('serverApi','http://api.atelieroom.test.5-soft.com/v1')
        .factory('handler',function($q){
            return {
                handleError : function(response) {
                    if (! angular.isObject(response.data) || ! response.data.message) {
                        return( $q.reject( "An unknown error occurred." ) );
                    }
                    return( $q.reject( response.data.message ) );
                },
                handleSuccess: function(response) {
                    return( response.data);
                }
            };
        })
        .factory('getAuth', function($auth){
            return {
                getAuth: function(){
                    return $auth.isAuthenticated();
                }
            };
        })
        .factory('InjectorAtelie',  function($rootScope,$q, $location){
            var authInjectorAtelie = {
                error: function (response) {
                    var status = response.status;

                    if (status === 401) {
                        //AuthFactory.clearUser();
                        //console.log('Non Auth');
                        $location.path('/logout');
                        return;
                    }
                // otherwise
                },
                response: function (response) {
                    // do something on error
                    if (response.status === 401) {
                       //console.log('Non Auth');
                        $location.path('/logout');
                    }
                    return response || $q.when(response);
                }
                /*responseError: function (rejection) {
                    if(rejection.status === 404){
                        $location.path('/404/');
                        return $q.reject(rejection);
                    }
                }*/
            };
            return authInjectorAtelie;
        })
        .factory('city',function($http,handler,$auth,$rootScope,serverApi){
            $rootScope.select_city = '';
            function getId(){
                var request = $http.get(serverApi+'/profiles/me');
                return request.then(handler.handleSuccess, handler.handleError);
            }
            function getCity()
            {
                var city = $http.get(serverApi+'/cities');
                return (city.then(handler.handleSuccess,handler.handleError));
            }
            return {
                getCity : function()
                {
                    if($auth.isAuthenticated()){
                        getId().then(function(response){
                                if (response.determination_city===null || response.determination_city===undefined){
                                    getCity().then(function(city){
                                        var select_city=city.filter(function(item){
                                            return item.id === response.user.profile.city_id;
                                        });
                                        $rootScope.select_city = (response.user.profile.city_id>0)?select_city[0].title:'-';
                                    });
                                }
                                else {
                                    $rootScope.select_city = response.determination_city.title;
                                }
                        });
                    }else{
                        $rootScope.select_city = '-';
                    }
                },
                getCities: function()
                {
                    return getCity();
                }
            };
        })
        .factory('menuItems', function($http, $q,handler,serverApi){
            return {
                getCategories : function()
                {
                    var categories = $http.get(serverApi+'/categories');
                    return (categories.then(handler.handleSuccess,handler.handleError));
                },
                getMenus: function()
                {
                    var request = $http.get(serverApi+'/pages/menu');
                    return (request.then(handler.handleSuccess, handler.handleError));
                }
            };
        })
        .service('socialAuth', function($http, $q,$window,$auth, handler,serverApi){
            var _socialObject = {},
                u_email = '',
                AppId=(window.location.hostname.indexOf('test.5-soft.com') !== -1)?"778566965337-3h1qcss5grqqffeh3kga0gkdbbl8oolo.apps.googleusercontent.com":"778566965337-7a7odgarq43msqrpi8utssfk781ri71r.apps.googleusercontent.com",
                clientSecret=(window.location.hostname.indexOf('test.5-soft.com') !== -1)?'QDD_S36yGFvGuRgiYsYto8Ee':'UYWgmMkUi52mb0YTGO52E2Z2',
                redirectUri=(window.location.hostname.indexOf('test.5-soft.com') !== -1)?'http://atelieroom.test.5-soft.com/users/login':'http://localhost:8000/users/default/slogin';
            var signIn = function () {
                var defered = $q.defer();
                $window.signinCallback = function (response) {
                    $window.signinCallback = undefined;
                    defered.resolve(response);
                };
                gapi.auth.signIn({
                    clientid: AppId,
                    cookiepolicy: "single_host_origin",
                    requestvisibleactions: "http://schemas.google.com/AddActivity",
                    scope: "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read",
                    callback: "signinCallback"

                });
                return defered.promise;
            };
            function handleSuccess(response)
            {
                return $.extend({},response.data,{'email':u_email});
            }
            return {
                setObjects: function(object)
                {
                    _socialObject = object;
                },
                getFacebookLink: function()
                {

                    u_email = _socialObject.email;
                   var request = $http.post(serverApi+'/user/authenticate?authclient=facebook',{'attributes':_socialObject});
                    return (request.then(handleSuccess, handler.handleError));
                 },
                getGoogleLink: function()
                {

                    return signIn().then(function(singInData){
                        var config ={
                            'client_id':singInData.clientid,
                            'client_secret':clientSecret,
                            'redirect_uri':redirectUri,
                            'grant_type':'authorization_code',
                            'code':singInData.code,
                            'access_token':singInData.access_token,
                            'headers':{
                                'Authorization':'Bearer '+singInData.access_token
                            }
                        };
                        var user_info=$http.get('https://www.googleapis.com/oauth2/v1/userinfo',config);
                        return (user_info.then(function(response){
                            return  $http.get('https://www.googleapis.com/plus/v1/people/'+response.data.id,config).then(function(people){
                                u_email = people.data.emails[0].value;
                                return $http.post(serverApi+'/user/authenticate?authclient=google',{'attributes':people.data}).then(handleSuccess);
                            });
                        }, handler.handleError));
                    });
                }
            };
        }).
        factory('confirmUser',function($http,$q,handler,serverApi){
                return{
                    send : function(data){
                        var request = $http.get(serverApi+'/user/confirm?id='+data.id+'&code='+data.code);
                        return (request.then(handler.handleSuccess,handler.handleError));
                    },
                    resend: function(email){
                        var request = $http.post(serverApi+'/user/resend',{'email':email});
                        return (request.then(handler.handleSuccess,handler.handleError));
                    }
                };
        })
        .factory('recoveryPassword',function($http, $q, handler, serverApi, $window){
            return {
                sendEmail : function(email)
                {
                    var request = $http.post(serverApi+'/user/request?link='+$window.location.origin,{'email': email});
                    return (request.then(handler.handleSuccess, handler.handleError));
                },
                resetPassword : function(id, code, password)
                {
                    var request = $http.post(serverApi+'/user/reset?'+$.param({'id': id,'code':code}),{'password':password});
                    return (request.then(handler.handleSuccess, handler.handleError));
                }
            };
        })
        .factory('formDataObject', function() {
            return function(data) {
                var fd = new FormData();
                angular.forEach(data, function(value, key) {
                    fd.append(key, value);
                });
                return fd;
            };
        })
        .factory('profile',function($http, $q, $auth, formDataObject,handler, $upload, serverApi){
            var u_id = 0;
            var untity_name = 'user';
            function IsNullOrEmpty(value)
            {
                return (value === null) ? "": value;
            }
            function handleSuccess( response ) {
                var t_resp = [];
                angular.forEach(response.data,function(key,val){
                    t_resp[val] = IsNullOrEmpty(key);
                });
                t_resp = $.extend({},t_resp,{'u_id':u_id,'entity_name':untity_name});
                return(t_resp);
            }
            function getId(){
                var request = $http.get(serverApi+'/profiles/me');
                return request.then(handler.handleSuccess, handler.handleError);
            }
            return {
                getCity: function()
                {
                    var city = $http.get(serverApi+'/cities');
                    return (city.then(handler.handleSuccess,handler.handleError));
                },
                getProfile: function() {
                    return getId().then(function(response){
                        u_id = response.user.id;
                        untity_name = response.user.entity_name;
                        var request = $http({
                            method: 'GET',
                            url: serverApi+'/profiles/'+response.user.id,
                            headers: {'Content-Type': 'application/json','Authorization': 'Bearer ' + $auth.getToken()}
                        });
                        return (request.then(handleSuccess, handler.handleError));
                    });
                },
                updateProfile: function(profileData) {
                return getId().then(function(response){
                    return $http.put(serverApi+'/profiles/'+response.user.id, $.param(profileData),{
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Authorization': 'Bearer ' + $auth.getToken()
                            }}
                        );
                    });
                },
                uploadImage: function(image,u_data)
                {
                    return $upload.upload({
                        url:serverApi+'/images',
                        method:'post',
                        data: u_data,
                        file: image,
                        fileName:'pic.png',
                        fileFormDataName:'fileUpload',
                        headers: {'Authorization': 'Bearer ' + $auth.getToken()}
                    });
                },
                register: function(type){
                    if (type){
                        var request = $http.get(serverApi+'/profiles/upgrade?to=specialist');
                        return (request.then(handleSuccess, handler.handleError));
                    }else{
                        var request = $http.get(serverApi+'/profiles/upgrade?to=seller');
                        return (request.then(handleSuccess, handler.handleError));
                    }
                }

            };
        })
        .factory('confirmAuth',function($http, $q ,handler){
            return {
                saveData: function(data, link)
                {
                    var request =  $http.post('http://api.atelieroom.test.5-soft.com'+link,data,{
                        headers:
                        {
                            'Content-Type':'application/json'
                        }
                    });
					return (request.then(handler.handleSuccess));
                }
            };
        })
        .factory('sliderObjects', function($http,handler,serverApi){
            return {
                getObjects: function(){
                    var objects = $http.get(serverApi+'/sliders');
                    return (objects.then(handler.handleSuccess, handler.handleError));
                }
            };
        })
        .factory('topSpecialist', function($http,$auth,handler,serverApi){
            function getCount()
            {
                var count = $http.get(serverApi+'/specialists/count');
                return (count.then(handler.handleSuccess, handler.handleError));
            }
            return {
                getCount : function()
                {
                    return getCount();
                },
                getSpecialists : function(index)
                {
                    var request = $http.get(serverApi+'/specialists/top?page='+(index-1),{
                        headers:{
                            'Authorization': 'Bearer '+$auth.getToken()
                        }
                    });
                    return (request.then(handler.handleSuccess, handler.handleError));
                }
            };
        })
        .factory('specialist',function($http,handler,serverApi){
            var city_title='-';
            function getCity()
            {

            }
            function handleSuccess( response ) {
                var t_resp = [];
                angular.forEach(response.data,function(key,val){
                    t_resp[val] = key;
                });
                return $http.get(serverApi+'/cities').then(function(city){
                        var city=city.data;
                        var select_city=city.filter(function(item){
                            return item.id === response.data.profile.city_id;
                        });
                        return $.extend({},t_resp,{'city_title':(response.data.profile.city_id>0)?select_city[0].title:'-'});
                    });

            }
            return{
                getProfile : function(username)
                {
                    var request = $http.get(serverApi+'/specialists/'+username,{},{'headers':{'ContentType':'application/x-www-form-urlencoded'}});
                    return (request.then(handleSuccess,handler.handleError));
                }
            };
        })
        .factory('Page',function($http, handler, serverApi){
            return {
                getPage : function(alias)
                {
                    var request = $http.get(serverApi+'/pages/'+alias);
                    return (request.then(handler.handleSuccess, handler.handleError));
                }
            };
        })
        .factory('Articles',function($http, handler, serverApi){
            var items = 4;
            return {
                mainArticles : function()
                {
                    var request = $http.get('/articles.json');
                    return (request.then(handler.handleSuccess, handler.handleError));
                },
                count : function(){
                    return this.mainArticles().then(function(response){
                        return (response.length%items>0?(parseInt(response.length/items)+1):response.length/items);
                    });
                },
                partsLoad: function(page)
                {
                    var page = (page-1) || 0;
                    var startRec = Math.max(page,0)*items;
                    var endRec = startRec+items;
                    return this.mainArticles().then(function(response){
                        return response.slice(startRec,endRec);
                    });
                },
                loadByAlias: function(alias)
                {
                    return this.mainArticles().then(function(response){
                        return response.filter(function(item){return item.alias === alias;})[0];
                    });
                }
            };
        });

});
