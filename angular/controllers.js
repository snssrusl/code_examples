'use strict';

define(['angular', 'services'], function (angular) {

    /* Controllers */

    return angular.module('myApp.controllers', ['ui.bootstrap','myApp.services','angucomplete-alt',
        'ngImgCrop','MessageCenterModule','ui.mask','ngDropdowns', 'ngAnimate', 'ui.tinymce','LocalStorageModule','at.multirange-slider'])

        .controller('MyCtrl1', ['$scope', 'version', function ($scope, version) {
            $scope.scopedAppVersion = version;
        }])
        .controller('CategoryMenuCtrl',function($scope,menuItems){
            menuItems.getCategories().then(function(items){
                $scope.categories = items;
            });
        })
        .controller('FooterCtrl', function(city,localStorageService,$scope,$rootScope){
            if (localStorageService.get('select-city')!==null){
                $rootScope.select_city =localStorageService.get('select-city');
            }else{
                city.getCity();
            }

            city.getCities().then(function(resp){
                $scope.cities = resp;
            });
            $scope.enableCity = function(){
                if ($scope.enableSelect===null || $scope.enableSelect===false){
                    $scope.enableSelect = true;
                }else{
                    $scope.enableSelect = false;
                }
            };
            $scope.selectCity = [];
            $scope.selectCity = function(selected) {
                localStorageService.set('select-city',selected.title);
                $rootScope.select_city = selected.title;
                $scope.enableSelect = false;
            };
            $scope.isHide = 0;

            if(localStorageService.get('footer-panel')!==null) {
                $scope.isHide = localStorageService.get('footer-panel');
            }else{
                localStorageService.set('footer-panel',$scope.isHide);
            }
            $scope.hideShow = function()
            {
                if (!$scope.isHide)
                {
                    localStorageService.set('footer-panel',1);
                    $scope.isHide = 1;
                }else{
                    localStorageService.set('footer-panel',0);
                    $scope.isHide = 0;
                }
            };
        })
        .controller('HomeCtrl', ['$scope', '$rootScope', '$auth','menuItems','topSpecialist', '$state','Articles',
            function ($scope, $rootScope, $auth, menuItems,topSpecialist,$state, Articles){
                $rootScope.$on("$locationChangeSuccess", function(){
                    window.scrollTo(0, 0);
                });

                $scope.isAuthenticated = function(){
                    return $auth.isAuthenticated();
                };


                topSpecialist.getCount().then(function(count){
                    $scope.cpecCount = count;
                });
                function getRange(arr,o,d)
                {
                    var data = [];
                    for(var i = 0;i<arr.length;i++) {
                        if (i >= o && i < d) {
                            data.push(arr[i]);
                        }
                    }
                    return data;
                }
                Articles.mainArticles().then(function(response){
                    var articles = response.splice(0,6);
                    var articlesColumn=[];
                    var counter=0;
                    for (var i = 0; i<6;i+=3)
                    {
                        articlesColumn[counter++]={
                            'big':getRange(articles,i,i+1),
                            'small':getRange(articles,i+1,i+3)
                        };
                    }
                    $scope.articlesColumn = articlesColumn;
                });
                if($state.current.name === ""){
                    $state.go('404');
                }
            }])
        .controller('PhotoCtrl',function($scope,profile){
            profile.getProfile().then(function(data) {
               $scope.userpic='http://backend.atelieroom.test.5-soft.com'+data.userpic;
            });
        })
        .controller('menuCtrl',function($scope, menuItems){
            menuItems.getMenus().then(function(response){
                $scope.header = response.header;
                var footer = response.footer;
                $scope.footer=[];
                var size = 6;
                var counter=0;
                for (var i=0; i<footer.length; i+=size) {
                    $scope.footer[counter++] = footer.slice(i,i+size);
                }
                if (counter<5) {
                    for (var k = counter; k < 5; k++) {
                        $scope.footer[k] = footer;
                    }
                }
            });
        })
        .controller('SignupCtrl', ['$scope', '$auth','messageCenterService','$timeout',  function ($scope, $auth,messageCenterService,$timeout){
            $scope.status=1;
            $scope.signup = function() {
                $scope.status=1;
                $auth.signup({
                    username: $scope.displayName,
                    email: $scope.email,
                    password: $scope.password
                })
                    .then(function(data){
                        $timeout(function(){
                            messageCenterService.add('success', 'Вы успешно зарегистрированы.<br>Проверьте почту', { html: true, timeout: 3000 , status: messageCenterService.status.permanent });
                        },100);
                    })
                    .catch(function(response) {
                        $scope.status=0;
                        $scope.msgs=response.data;
                        messageCenterService.add('danger', 'Ошибка регистрации', { html: true, timeout: 3000 });
                    });
            };
        }])
        .controller('LogoutCtrl', ['$scope', '$auth', function ($scope, $auth){
            if (!$auth.isAuthenticated()) {
                return;
            }
            $auth.logout()
                .then(function() {

                });

        }])
        .controller('ConfirmUserCtrl',['$scope','confirmUser','$state','messageCenterService','$timeout','$location',function($scope, confirmUser,$state,messageCenterService,$timeout,$location){
            confirmUser.send($location.search()).then(function(response){
                $timeout(function(){
                    messageCenterService.add('success', 'Пользователь успешно подтвержден', { html: true, timeout: 3000 , status: messageCenterService.status.permanent });
                },100);
                $state.go('home');
            },function(){
                messageCenterService.add('danger', 'Ссылка устрела<br>Повторите еще запрос', { html: true, timeout: 3000 });
            });
            $scope.resend = function()
            {
                confirmUser.resend($scope.email).then(function(response){
                    if (response.success===false){
                        messageCenterService.add('danger', response.errors.email[0], { html: true, timeout: 3000 , status: messageCenterService.status.permanent });
                    }else {
                        messageCenterService.add('success', 'Проверьте почту', {
                            html: true,
                            timeout: 3000,
                            status: messageCenterService.status.permanent
                        });
                    }
                });
            };

        }])
        .controller('LoginCtrl', ['$scope', '$auth', 'socialAuth','$modal','$rootScope','messageCenterService','$timeout',
            function ($scope,$auth,socialAuth, $modal, $rootScope,messageCenterService,$timeout){
                $scope.status=1;
                $scope.loginn = function() {
                    $auth.login({ login: $scope.email, password: $scope.password })
                        .then(function(response) {
                            $scope.status=1;
                            $scope.$apply();
                            $rootScope.$digest();
                            $timeout(function(){
                                messageCenterService.add('success', 'Вы успешно авторизованы', { html: true, timeout: 3000 });
                            },100);
                        })
                        .catch(function(response) {
                            $scope.status=0;
                            $scope.msgs=response.data;
                            messageCenterService.add('danger', 'Ошибка авторизации', { html: true, timeout: 3000 });

                        });
                };
                $scope.modal_confirm = function (resp) {
                    var scope =     $rootScope.$new();
                    scope.params = {linkAuth : resp.step2, email : resp.email};
                    var modalInstance = $modal.open({
                        templateUrl: 'app/partials/confirm_modal.html',
                        controller: 'ConfirmAuthCtrl',
                        size: 100,
                        scope: scope
                    });
                };
                $scope.authGoogle = function()
                {
                    socialAuth.getGoogleLink().then(function(response){

                        if (response.hasOwnProperty('token')){
                            $auth.setToken(response.token);
                            $timeout(function(){
                                $scope.$apply();
                                $rootScope.$digest();
                                messageCenterService.add('success', 'Вы успешно авторизованы', { html: true, timeout: 3000 });
                            },100);

                        }else
                        {
                            $scope.modal_confirm(response);
                        }
                    });

                };
                $scope.authFacebook = function(){
                    FB.login(function(response){
                        if (response.status === 'connected'){
                            FB.api('/me', function(response){
                                socialAuth.setObjects(response);
                                socialAuth.getFacebookLink().then(function(response){
                                    if (response.hasOwnProperty('token')){
                                        $auth.setToken(response.token);
                                        $scope.$apply();
                                        $rootScope.$digest();
                                        $timeout(function(){
                                            messageCenterService.add('success', 'Вы успешно авторизованы', { html: true, timeout: 3000 });
                                        },100);
                                    } else {
                                        $scope.modal_confirm(response);
                                    }
                                });
                            });
                        }
                    }, {scope:'publish_actions, email,user_likes'});
                };

            }])
        .controller('RecPassCtrl',function($scope,recoveryPassword,messageCenterService){
            function implode( glue, pieces ) {
                return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
            }
            $scope.sendEmail = function()
            {
                recoveryPassword.sendEmail($scope.email).then(function(response){
                    if (!response.hasOwnProperty('errors')){
                        messageCenterService.add('success', implode('<br>',response.messages), { html: true, timeout: 3000 });
                    }else
                    {
                        messageCenterService.add('danger',response.errors.email[0], { html: true, timeout: 3000 });
                    }
                });
            };

        })
        .controller('ConfirmResetCtrl',function($scope,recoveryPassword,messageCenterService,$location, $window, $timeout,$state){
            function implode( glue, pieces ) {
                return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
            }
            var params = $location.search();
            if (!params.hasOwnProperty('id') && !params.hasOwnProperty('code')) {
                $window.location.href = '/';
            }
            $scope.resetConfirm = function()
            {
                recoveryPassword.resetPassword(params.id,params.code,$scope.password).then(function(response){
                    if (!response.hasOwnProperty('errors')){
                        messageCenterService.add('success', implode('<br>',response.messages), { html: true, timeout: 3000 });
                        $timeout(function(){
                            $window.location.href='/#/login';
                        },3000);
                    }else
                    {
                        messageCenterService.add('danger',response.errors.email[0], { html: true, timeout: 3000 });
                    }
                },function(){
                    $state.go('404');
                });
            };
        })
        .controller('ProfileCtrl',function($scope, profile,menuItems, $modal, $rootScope,messageCenterService,city,$timeout){
            var userpic={};
            var u_id = 0;
            $scope.month = [
                {text:'Январь',     value:1},
                {text:'Февраль',    value:2},
                {text:'Март',       value:3},
                {text:'Апрель',     value:4},
                {text:'Май',        value:5},
                {text:'Июнь',       value:6},
                {text:'Июль',       value:7},
                {text:'Август',     value:8},
                {text:'Сентябрь',   value:9},
                {text:'Октябрь',    value:10},
                {text:'Ноябрь',     value:11},
                {text:'Декабрь',    value:12}
            ];
            $scope.years = range(1950,2014).reverse();
            $scope.days =  range(1,31);
            $scope.b_day = {};
            $scope.b_month={};
            $scope.b_year={};
            $scope.getProfile = function(){
                profile.getProfile().then(function(data){
                    profile.getCity().then(function(city){
                        $rootScope.imgCropped = 'http://backend.atelieroom.test.5-soft.com/'+data.userpic;
                        u_id = data.u_id;
                        $scope.user = data;
                        $scope.cities = city;
                        var select_city=city.filter(function(item){
                            return item.id === data.city_id;
                        });
                        $scope.select_city_contact = (data.city_id>0)?select_city[0].title:$rootScope.select_city;
                    });
                    setGender(data);
                    var birthday = new Date(data.birthday);
                    $scope.b_day = {text:$scope.days.filter(function(item){return item.value===birthday.getDate()||1;})[0].text,value:$scope.days.filter(function(item){return item.value===birthday.getDate()||1;})[0].value};
                    $scope.b_month = {text:$scope.month.filter(function(item){return item.value===(birthday.getMonth()+1)||1;})[0].text,value:$scope.month.filter(function(item){return item.value===(birthday.getMonth()+1)||1;})[0].value};
                    $scope.b_year = {text:$scope.years.filter(function(item){return item.value===birthday.getFullYear()||2014;})[0].text,value:$scope.years.filter(function(item){return item.value===birthday.getFullYear()||2014;})[0].value};
                    city.getCity();
                });
            };
            var dataURItoBlob = function(dataURI) {
                var binary = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
                var array = [];
                for(var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], {type: mimeString});
            };
            $scope.updateProfile = function()
            {
                $scope.user.birthday = parseDate($scope.b_year.value,$scope.b_month.value,$scope.b_day.value);
                if ($scope.hasOwnProperty('selectedCity'))
                {
                    $scope.user.city_id=$scope.selectedCity.originalObject.id;
                }
                profile.updateProfile($scope.user).then(function(data){
                    if (userpic.hasOwnProperty('name')){
                        profile.uploadImage(dataURItoBlob($rootScope.imgCropped),{'entity_id':u_id,'entity_name':'user'}).then(function(imgresp){
                            $scope.user.userpic = imgresp.data.file;
                        });
                    }
                    city.getCity();
                    messageCenterService.add('success', 'Данные успешно сохранены', { html: true, timeout: 3000 });
                });
            };
            $scope.registerSpec = function()
            {
                profile.register(1).then(function(data){
                    $scope.user.entity_name='specialist';
                    $scope.getProfile();
                });
            };
            $scope.registerSeller = function()
            {
                profile.register(0).then(function(data){
                    $scope.user.entity_name='seller';
                    $scope.getProfile();
                });
            };
            $scope.entityName = {'user':'Пользователь','specialist':'Специалист','seller':'Продавец'};
            menuItems.getCategories().then(function(items){
                $scope.categories = items;
            });
            function setGender(data)
            {
                if (data.gender!=='')
                {
                    if (data.gender==='male'){
                        $scope.gender_male = 1;
                        $scope.gender_female = 0;
                    }
                    else if (data.gender==='female'){
                        $scope.gender_female = 1;
                        $scope.gender_male = 0;
                    }else
                    {
                        $scope.gender_male = 0;
                        $scope.gender_shemale = 0;
                    }

                }else
                {
                    $scope.gender_male = 0;
                    $scope.gender_shemale = 0;
                }
            }
            function parseDate(year, month, day)
            {
                year=isNaN(parseInt(year))?'0000':year;
                month=isNaN(parseInt(month))?'00':month;
                day = isNaN(parseInt(day))?'00':day;
                return year+'-'+month+'-'+day;
            }
            function range(min, max){
                var input = [];
                for (var i = min; i <= max; i += 1) {
                    input.push({text: i, value: i});
                }
                return input;
            }
            $scope.fileChanged = function(element)
            {
                var photofile=element.files[0];
                var reader = new FileReader();
                var imageParams = new Image();
                if (photofile.type.search('image')===-1) {
                    $timeout(function(){
                        messageCenterService.add('danger', 'Неверный формат изображения', {html: true,timeout: 3000});
                    },100);
                }else{
                    reader.onload = function(e) {
                        imageParams.src = e.target.result;
                        if (imageParams.height > 10000 || imageParams.width > 10000){
                            $timeout(function(){
                                messageCenterService.add('danger', 'Размер изображения превышает 10000x10000', {html: true,timeout: 3000});
                            },100);
                        }else if(photofile.size > 2072576){
                            $timeout(function(){
                                messageCenterService.add('danger', 'Размер изображения превышает 2Мб', { html: true, timeout: 3000 });
                            },100);
                        }else{
                            $scope.$apply(function() {

                                $rootScope.params = {prev_img: e.target.result};
                                $modal.open({
                                    templateUrl: 'app/partials/edit_image.html',
                                    controller: 'ImageCropCtrl',
                                    size: 'lg'
                                });
                            });
                            userpic=element.files[0];
                        }
                    };
                }
                reader.readAsDataURL(photofile);
            };
            $scope.changeGender = function(m,f)
            {
                $scope.gender_male=m;
                $scope.gender_female=f;
                if (m===1) {
                    $scope.user.gender = 'male';
                }else{
                    $scope.user.gender = 'female';
                }
            };
            $scope.getProfile();
        })
        .controller('ConfirmAuthCtrl',['$scope','$auth','confirmAuth','$window','$modalInstance',
            function($scope,$auth,confirmAuth,$window,$modalInstance){
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.status=0;
                $scope.messages={};
                $scope.user = {email:$scope.params.email};
                $scope.save = function()
                {
                    confirmAuth.saveData($scope.user,$scope.params.linkAuth).then(function(response){
                        if(response.hasOwnProperty('token')){
                            $auth.setToken(response.token);
                            $modalInstance.close();
                        }
                    },function(response,status){
                        $scope.status=1;
                        $scope.messages=response.data;
                    });
                };
            }])
        .controller('sliderController', ['$scope', '$timeout', function($scope, $timeout){
            var i = 0,
                width = 1290,
                height = 580;
            var canvas = new fabric.Canvas('sliderCanvas');
            canvas.setWidth(width);
            canvas.setHeight(height);
            var imgPath = 'app/img/slider/';
            var imgSrcArr = ['1.jpg', '2.jpg', '3.jpg'];
            var quantityObjects = imgSrcArr.length;
            for (i = 0; i < quantityObjects; ++i){
                var addImg = function(k){
                    var imgFabric = fabric.Image.fromURL(imgPath+imgSrcArr[k], function (oImg) {

                        oImg.set('left',(k===0)?0:-(width*k));
                        oImg.scale(1);
                        oImg.hasControls = false;
                        oImg.hasBorders = false;
                        oImg.selectable = false;
                        canvas.add(oImg);
                        //canvas.renderAll();
                        animateImg(oImg,k);
                    });
                };
                addImg(i);
            }
            var animateImg = function(oImg, k){
                var startAnimate = function () {
                    var leftCoord = 0,
                        shift=0;
                    if (oImg.left === 0){
                        shift = width;
                    } else {
                        shift = oImg.left + width;
                    }
                    //console.log(oImg.left);
                    //console.log(oImg.toString());
                    oImg.animate('left', shift, {
                        duration: 3000,
                        onChange: canvas.renderAll.bind(canvas),
                        onComplete: function(){
                            if(shift >= width){
                                shift = -(quantityObjects-1)*width;
                                oImg.set('left',shift);
                            }
                        }
                    });
                    $timeout(startAnimate, $scope.timeChange);

                };

                $timeout(startAnimate,$scope.timeChange);
            }

            canvas.renderAll();
            console.log(canvas);
        }])
        .controller('CarouselCtrl', ['$scope', 'sliderObjects', function($scope,sliderObjects){
            var slides = $scope.slides = [],
                imgPath = 'http://backend.atelieroom.test.5-soft.com/',
                images = [],
                texts = [],
                i = 0;
            $scope.myInterval = 5000;
            sliderObjects.getObjects().then(function(response){
                console.log(response);
                for(i = 0; i<response.length;++i){
                    console.log(response[i]);
                    images.push({image:imgPath+response[i].image, text:response[i].text, link:response[i].link});
                }
                for (i=0; i<response.length; i++) {
                    $scope.addSlide(images[i]);
                }
            });

            $scope.addSlide = function(item) {
                var newWidth = 600 + slides.length + 1;
                slides.push(item);
            };

        }])
        .controller('ImageCropCtrl',function($scope,$window,$modalInstance, $rootScope){
            $scope.myImage='';
            $scope.myCroppedImage='';
            $scope.myImage = $rootScope.params.prev_img;

            $scope.save = function()
            {
                $rootScope.imgCropped = $scope.myCroppedImage;
                $modalInstance.close();
            };
        })
        .controller('topSpecialist',['$scope','topSpecialist','$timeout',function($scope,topSpecialist,$timeout){
            var current_page = 1;
            var page_count = 1;
            topSpecialist.getSpecialists(current_page).then(function(response){
                $scope.specialists = response;
            });
            topSpecialist.getCount().then(function(count_users){
                page_count=(count_users.count%5)?parseInt((count_users.count)/5)+1:parseInt((count_users.count)/5);
            });
            $scope.left = function()
            {
                if (current_page===1) {
                    current_page = page_count;
                }else if(current_page>1){
                    current_page--;
                }
                $timeout(function(){
                    topSpecialist.getSpecialists(current_page).then(function(response){
                        $scope.specialists = response;
                    });
                });
            };
            $scope.right = function()
            {
                if (current_page===page_count) {
                    current_page = 1;
                }else if(current_page<page_count){
                    current_page++;
                }
                $timeout(function(){
                    topSpecialist.getSpecialists(current_page).then(function(response){
                        $scope.specialists = response;
                    });
                });
            };
        }])
        .controller('SpecialistsCtrl',['$scope','specialist','city','$rootScope',function($scope,specialist,city,$rootScope){
            city.getCities().then(function(city){
                $scope.cities = city;
                $scope.select_city_contact=$rootScope.select_city;
            });
        }])
        .controller('SpecialistCtrl',['$scope','specialist','$state',function($scope,specialist,$state){
            $scope.isCollapsed = true;
            $scope.getContact = function()
            {
                specialist.getProfile($state.params.username).then(function(response){
                    $scope.contact = response;
                    $scope.isCollapsed = !$scope.isCollapsed;
                });
            }

            specialist.getProfile($state.params.username).then(function(response){
                $scope.profile = response;
            });
        }])
        .controller('PageCtrl',function($scope, Page, $state){
            if ($state.params.alias===''){
                $state.go('404');
            }
            Page.getPage($state.params.alias).then(function(page){
                $scope.page = page;
            },function(){
                $state.go('404');
            });
        })
        .controller('ArticlesCtrl',function($scope,Articles){
            var current_page = 1;
            $scope.articles = [];
            function getParts(current_page){
                Articles.partsLoad(current_page).then(function(response){
                    $scope.articles = response;
                });
                window.scrollTo(0,0);
            }
            Articles.count().then(function(count){
                if (count>0){
                    var temp_arr=[];
                    for(var i = 1; i<=count;i++){
                        temp_arr.push(i);
                    }
                    $scope.pagination = temp_arr;
                }
            });
            $scope.pageLoad = function(id)
            {
                current_page=id;
                getParts(id);
            };
            $scope.right = function()
            {
                Articles.count().then(function(count) {
                        if (current_page >= 1 && current_page<count){
                            current_page=current_page+1;
                            getParts(current_page);
                        }else{
                            current_page=1;
                            getParts(current_page);
                        }

                    }
                );
            };
            $scope.left = function()
            {
                Articles.count().then(function(count) {
                        if (current_page > 1 && current_page<=count) {
                            getParts(current_page--);
                        }else
                        {
                            current_page=count;
                            getParts(current_page);
                        }

                    }
                );
            };
            getParts($scope.current_page);
        })
        .controller('ArticleCreate', function($scope){
            console.log('Article Create');
            $scope.modelTinyMCE = 'one';

        })
        .controller('ArticleCtrl',['$scope', 'Articles','$state',function($scope,Articles,$state){
                Articles.loadByAlias($state.params.alias).then(function(response){
                    $scope.article = response;
                });
        }])
        .controller('CategoriesCtrl',['$scope','$sce',function($scope,$sce){
            $scope.otherProbs = {
                key1: {
                    p: 3
                },
                key2: {
                    p: 5
                },
                key4: {
                    p: 6
                }
            };
        }])
        .controller('MyCtrl2', ['$scope', '$injector', function($scope, $injector) {
            require(['controllers/myctrl2'], function(myctrl2) {
                // injector method takes an array of modules as the first argument
                // if you want your controller to be able to use components from
                // any of your other modules, make sure you include it together with 'ng'
                // Furthermore we need to pass on the $scope as it's unique to this controller
                $injector.invoke(myctrl2, this, {'$scope': $scope});
            });
        }])
        .controller('404Ctrl', ['$scope', function($scope){

        }]);
});
