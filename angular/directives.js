'use strict';

define(['angular', 'services'], function(angular, services) {

    /* Directives */

    angular.module('myApp.directives', ['myApp.services', 'myApp.controllers'])
        .directive('appVersion', ['version', function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }])
        .directive('disableNgAnimate', ['$animate', function($animate) {
            return {
                restrict: 'A',
                link: function(scope, element) {
                    $animate.enabled(false, element);
                }
            };
        }])
        .directive('sliderCanvasDirective',[function(){
            return {
                restrict: 'EA',
                controller: 'sliderController',
                scope: {
                    duration: '@',
                    timeChange: '@'
                },
                compile: function(){
                   /* var canvas = new fabric.Canvas('sliderCanvas');
                    var imgPath = 'app/img/slider/'
                    var imgSrcArr = ['1.jpg', '2.jpg', '3.jpg'];
                    var imgFabric = fabric.Image.fromURL(imgPath+imgSrcArr[0], function (oImg) {
                        canvas.add(oImg);
                        oImg.set('left',0);
                        console.log('img add');
                    });
                    canvas.renderAll();
                    console.log(canvas);*/
                },
                link: function(){
                    console.log($scope.timeChange);

                }
            };
        }])

        .directive('sliderr',  [function() {

            return {
                restrict: 'E',
                replace: true,
                template: '<div class="fabricJSslider">This is slider</div>'
            };
        }])
        .directive('fileModel',function($parse){
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    var model = $parse(attrs.fileModel);
                    var modelSetter = model.assign;
                    element.bind('change', function(changeEvent){
                        scope.$apply(function(){
                            modelSetter(scope, element[0].files[0]);
                        });
                        console.log(changeEvent.target.files[0]);
                    });
                }
            };
        })
        .directive('fileButton', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attributes) {
                    var el = angular.element(element)
                    var button = el.children()[0]
                    el.css({
                        position: 'relative',
                        overflow: 'hidden'
                    });
                    var fileInput = angular.element('<input accept="image/jpeg,image/png,image/bmp" type="file" class="fileInput"  onchange="angular.element(this).scope().fileChanged(this)" />');
                    el.append(fileInput);
                }
            };
        })
        .directive('googlePlusSignin', ['$window' ,'$rootScope', function ($window,$rootScope) {
            var ending = /\.apps\.googleusercontent\.com$/;
            return {
                restrict: 'A',
                transclude: true,
                //template: '<a href="#"><img src="/img/googleplus_32.png"/></a>',
                template: '<span class="ion-social-googleplus"></span>Войти через Google',
                //replace: true,
                link: function (scope, element, attrs, ctrl, linker) {
                    attrs.clientid += (ending.test(attrs.clientid) ? '' : '.apps.googleusercontent.com');

                    attrs.$set('data-clientid', attrs.clientid);

                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                    po.src = 'https://apis.google.com/js/client:platform.js?onload=render';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

                    var defaults = {
                        callback: 'signinCallback',
                        cookiepolicy: 'single_host_origin',
                        requestvisibleactions: 'http://schemas.google.com/AddActivity',
                        scope: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
                        width: 'wide'
                    };
                    defaults.clientid = attrs.clientid;
                    element.on('click',function(){
                        gapi.auth.signIn(defaults);
                    });
                    $window.signinCallback = function(authResult) {
                        if (authResult['status']['signed_in']) {
                            $rootScope.$broadcast('event:google-plus-signin-success', authResult);
                        }
                    };
                }
            };
        }]).directive('angularValidator',
        function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs, fn) {
                    var DOMForm = angular.element(element)[0];
                    var scopeForm = scope[DOMForm.name];
                    scopeForm.submitted = false;
                    element.on('submit', function(event) {
                        event.preventDefault();
                        scope.$apply(function() {
                            scopeForm.submitted = true;
                        });
                        if (scopeForm.$valid) {
                            scope.$apply(function() {
                                scope.$eval(DOMForm.attributes["angular-validator-submit"].value);
                            });
                        }
                    });
                    setupWatches(DOMForm);
                    function setupWatches(formElement) {
                        for (var i = 0; i < formElement.length; i++) {
                            // This ensures we are only watching form fields
                            if (i in formElement) {
                                setupWatch(formElement[i]);
                            }
                        }
                    }
                    function setupWatch(elementToWatch) {
                        if ("validate-on" in elementToWatch.attributes && elementToWatch.attributes["validate-on"].value === "blur") {
                            angular.element(elementToWatch).on('blur', function() {
                                updateValidationMessage(elementToWatch);
                                updateValidationClass(elementToWatch);
                            });
                        }

                        scope.$watch(function() {
                                return elementToWatch.value + scopeForm.submitted + checkElementValididty(elementToWatch) + getDirtyValue(scopeForm[elementToWatch.name]);
                            },
                            function() {
                                if ("validate-on" in elementToWatch.attributes && elementToWatch.attributes["validate-on"].value === "dirty") {
                                    updateValidationMessage(elementToWatch);
                                    updateValidationClass(elementToWatch);
                                }
                                else if (scopeForm.submitted || (scopeForm[elementToWatch.name] && scopeForm[elementToWatch.name].$valid)) {
                                    updateValidationMessage(elementToWatch);
                                    updateValidationClass(elementToWatch);
                                }
                            });
                    }
                    function getDirtyValue(element) {
                        if (element) {
                            if ("$dirty" in element) {
                                return element.$dirty;
                            }
                        }
                    }

                    function checkElementValididty(element) {
                        if ("validator" in element.attributes) {
                            var isElementValid = scope.$eval(element.attributes.validator.value);
                            scopeForm[element.name].$setValidity("angularValidator", isElementValid);
                            return isElementValid;
                        }
                    }
                    function updateValidationMessage(element) {
                        var defaultRequiredMessage = function() {
                            return "<i class='fa fa-times'></i> Required";
                        };
                        var defaultInvalidMessage = function() {
                            return "<i class='fa fa-times'></i> Invalid";
                        };
                        if (!(element.name in scopeForm)) {
                            return;
                        }
                        var scopeElementModel = scopeForm[element.name];
                        if (scopeElementModel.$dirty || scope[element.form.name].submitted) {
                            var validationMessageElement = isValidationMessagePresent(element);
                            if (validationMessageElement) {
                                validationMessageElement.remove();
                            }
                            if (scopeElementModel.$error.required) {
                                if ("required-message" in element.attributes) {
                                    angular.element(element).after(generateErrorMessage(element.attributes['required-message'].value));
                                }
                                else {
                                    angular.element(element).after(generateErrorMessage(defaultRequiredMessage));
                                }
                            } else if (!scopeElementModel.$valid) {
                                if ("invalid-message" in element.attributes) {
                                    angular.element(element).after(generateErrorMessage(element.attributes['invalid-message'].value));
                                }
                                else {
                                    angular.element(element).after(generateErrorMessage(defaultInvalidMessage));
                                }
                            }
                        }
                    }
                    function generateErrorMessage(messageText) {
                        return "<label class='control-label has-error validationMessage'>" + scope.$eval(messageText) + "</label>";
                    }
                    function isValidationMessagePresent(element) {
                        var elementSiblings = angular.element(element).parent().children();
                        for (var i = 0; i < elementSiblings.length; i++) {
                            if (angular.element(elementSiblings[i]).hasClass("validationMessage")) {
                                return angular.element(elementSiblings[i]);
                            }
                        }
                        return false;
                    }
                    function updateValidationClass(element) {
                        if (!(element.name in scopeForm)) {
                            return;
                        }
                        var formField = scopeForm[element.name];
                        if (formField.$dirty || scope[element.form.name].submitted) {
                            if (formField.$valid) {
                                angular.element(element.parentNode).removeClass('has-error');
                                angular.element(element).removeClass('has-error');
                            } else if (formField.$invalid) {
                                angular.element(element.parentNode).addClass('has-error');
                                angular.element(element).addClass('has-error');
                            }
                        }
                    }

                }
            };
        }
    )
        .directive('socialShare',function($window){
            return {
                restrict: 'A',
                scope:{
                    socialShare:'@'
                },
                controller: function($scope){
                },
                link :function(scope, element, attrs, fn){
                    var url='';
                    var title = attrs.socialShare;
                    var classes=attrs.class;
                    if (classes.search('facebook')!=-1){
                        url='http://www.facebook.com/share.php?'+ $.param({'u':$window.location.href,'title':title});
                    }else if(classes.search('gplus')!=-1){
                        url='https://plus.google.com/share?'+ $.param({'url':$window.location.href});
                    }else if (classes.search('twitter')!=-1){
                        url='http://twitter.com/home?status='+title+'+'+$window.location.href;
                    }else if (classes.search('vk')!=-1){
                        url='http://vk.com/share.php?url='+$window.location.href;
                    }
                    element.on('click',function(){
                        if (url!='')
                        $window.open(url,'Поделиться', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=600,width=600');
                    });
                }
            }
        })
        .directive('facebookLike',function($window){
            var params={
                href:'https://www.facebook.com/facebook',
                width:293,
                height:290,
                colorscheme:'light',
                show_faces:true,
                header:true,
                stream:false,
                show_border:true,
                appId:(window.location.hostname.indexOf('test.5-soft.com') != -1)? '727118950697004' : '580232505409844'
            }
            var url='//www.facebook.com/plugins/likebox.php?'+ $.param(params);
            return{
                restrict:'E',
                template:'<iframe src="'+url+'" scrolling="no"frameborder="0"style="border:none; overflow:hidden; height:290px;" width="293px" allowTransparency="true"></iframe>',
                replace:false
            };
        });
            
});
