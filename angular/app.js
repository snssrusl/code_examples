'use strict';

define([
	'angular',
	'filters',
	'services',
	'directives',
	'controllers',
	'angularRoute',
	'bootstrap',
	'uibootstrap',
	'uirouter',
	'satellizer',
    'angucomplete-alt',
	'fabricjs',
    'fileUpload',
    'ngImgCrop',
    'alerts',
	'angular-loading-bar',
	'ui-utils',
	'angular-dropdowns',
	'ngAnimate',
	'ui-tinymce',
	'angular-local-storage',
	'angular-multirange-slider'
	//'jquery'

	], function (angular, filters, services, directives, controllers,bootstrap,less,satellizer) {

		// Declare app level module which depends on filters, and services
		
		return angular.module('myApp', [
			'ngRoute',
			'ui.router',
			'myApp.filters',
			'myApp.services',
			'myApp.directives',
			'myApp.controllers',
			'satellizer',
            'angucomplete-alt',
            'ngImgCrop',
			'angular-loading-bar',
			'ui.tinymce',
			//'ui-utils'
           // 'growlNotifications'
		]);
});
