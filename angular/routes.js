'use strict';

define(['angular', 'app', 'require'], function(angular, app, require) {

	return app.config(['$routeProvider', '$stateProvider', '$authProvider', '$locationProvider', '$httpProvider',
        function($routeProvider, $stateProvider, $authProvider,$locationProvider,$httpProvider) {

		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
		var apiServer = 'http://api.atelieroom.test.5-soft.com';
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'app/partials/main.html',
				controller: 'HomeCtrl'
			})
			.state('login', {
				url: '/login',
				templateUrl: 'app/partials/login.html',
				controller: 'LoginCtrl'
			})
			.state('logout', {
				url: '/logout',
				template: null,
				controller: 'LogoutCtrl'
			})
			.state('signup', {
				url: '/signup',
				templateUrl: 'app/partials/signup.html',
				controller: 'SignupCtrl'
			})
            .state('recovery',{
                url:'/recovery',
                templateUrl: 'app/partials/recovery.html',
                controller: 'RecPassCtrl'
            })
            .state('reset/:id/:code',{
                url:'/reset',
                templateUrl: 'app/partials/confirm_reset.html',
                controller: 'ConfirmResetCtrl'
            })
			.state('confirm/:id/:code',{
				url:'/confirm',
				templateUrl: 'app/partials/confirm_account.html',
				controller: 'ConfirmUserCtrl'
			})
			.state('offer', {
				url: '/offer',
				templateUrl: 'app/partials/offer.html'
			})
			.state('specialists',{
				url: '/specialists',
				templateUrl: 'app/partials/specialists.html',
                controller:'SpecialistsCtrl'
			})
			.state('specialist',{
				url: '/specialist/:username',
				templateUrl: '/app/partials/specialist.html',
                controller:'SpecialistCtrl'
			})
			.state('articles',{
				url: '/articles',
				templateUrl: 'app/partials/articles.html',
                controller:'ArticlesCtrl'
			})
			.state('articles.new', {
				url: '/new',
				templateUrl: 'app/partials/articles.new.html',
				controller: 'ArticleCreate'
			})
            .state('article',{
				url: '/article/:alias',
				templateUrl: '/app/partials/article.html',
                controller:'ArticleCtrl'
			})
			.state('goods',{
				url: '/goods',
				templateUrl: 'app/partials/goods.html'
			})
			.state('designs',{
				url: '/designs',
				templateUrl: 'app/partials/designs.html'
			})
			.state('404',{
				url: '/404',
				templateUrl: '/app/partials/404.html'
			})
			.state('page',{
				url:'/page/:alias',
				templateUrl:'/app/partials/page.html',
				controller: 'PageCtrl'
			})
			.state('categories', {
				url:'/categories',
				templateUrl: '/app/partials/categories.html',
				controller: 'CategoriesCtrl'
			})
			.state('nested',{
				url:'/nested',
				template:'<body><ui-view class="myclass">'+
							 '<i>Some content will load here!</i>' +
							 '</ui-view></body>'
			})
			.state('nested.new',{
				url:'/new',
				templateUrl:'app/partials/recovery.html'
			})
			.state('profile', {
				url: '/profile',
				templateUrl: 'app/partials/profile.html',
				controller: 'ProfileCtrl'

			});

		//$facebookProvider.setAppId('727118950697004');
		$routeProvider
			.when('/login',
			{
				templateUrl: 'app/partials/login.html',
				controller: 'LoginCtrl'
			})
			.when('/signup', {
				templateUrl: 'app/partials/signup.html',
				controller: 'SignupCtrl'
			}).when('/404',
			{
				tepmplateUrl: 'app/partials/404.html',
				controller: '404Ctrl'
			});
		$routeProvider.otherwise('/404');

		$authProvider.loginUrl = 'http://api.atelieroom.test.5-soft.com/v1/user/login';
		$authProvider.signupUrl = 'http://api.atelieroom.test.5-soft.com/v1/user/register?link='+window.location.origin;
		$authProvider.httpInterceptor = false;
		$httpProvider.interceptors.push('InjectorAtelie');


	}]);

});
