'use strict';

require.config({
	paths: {
		angular: '../../bower_components/angular/angular',
		angularRoute: '../../bower_components/angular-route/angular-route.min',
		angularMocks: '../../bower_components/angular-mocks/angular-mocks',
		uibootstrap: '../../bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
		text: '../../bower_components/requirejs-text/text',
		bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap.min',
		jquery: '../../bower_components/jquery/dist/jquery.min',
		uirouter: '../../bower_components/angular-ui-router/release/angular-ui-router',
		satellizer: '../../bower_components/satellizer/satellizer',
		'angucomplete-alt': '../../bower_components/angucomplete-alt/angucomplete-alt',
		'facebook': '//connect.facebook.net/en_US/all',
		'google':'https://apis.google.com/js/client:platform.js?onload=render',
		fabricjs: '../../bower_components/fabric/dist/fabric',
        'fileUpload': 'fileUpload',
        'ngImgCrop': '../../bower_components/ngImgCrop/compile/minified/ng-img-crop',
        ngAnimate:'../../bower_components/angular-animate/angular-animate.min',
        alerts:'alerts',
		'angular-loading-bar':'../../bower_components/angular-loading-bar/build/loading-bar',
		'ui-utils':'../../bower_components/angular-ui-utils/ui-utils.min',
		'angular-dropdowns':'../../bower_components/angular-dropdowns/dist/angular-dropdowns',
		'ui-tinymce': '../../bower_components/angular-ui-tinymce/src/tinymce',
		'tinymce': '../../bower_components/tinymce/tinymce.min',
		'angular-local-storage': '../../bower_components/angular-local-storage/dist/angular-local-storage',
		'angular-multirange-slider': '../../bower_components/angular-multirange-slider/dist/multirange-slider'
	},
	shim: {
		'jquery' : {'exports':  'jquery'},
		'bootstrap' : ['jquery'],
		'angular' : {'exports' : 'angular'},
		'angularRoute': ['angular'],
		'angularMocks': {
			deps:['angular'],
			'exports':'angular.mock'
		},
		uibootstrap: {
			deps:['angular']
		},
		uirouter: {
			deps:['angular']
		},
		satellizer: {
			deps: ['angular']
		},
        'angucomplete-alt':{
            deps:['angular']
        },
        'fileUpload':{
            deps:['angular'],
           'exports':'fileUpload'
        },
        'ngImgCrop':{
            deps :['angular'],
            'exports':'ng-img-crop'
        },
        ngAnimate:{
            deps :['angular']
            //'exports':'angular-animate'
        },
        alerts:
        {
            deps: ['angular']
        },
		'angular-loading-bar': {
			deps: ['angular']
		},
		'ui-utils': {
			deps: ['angular']
		},
		'angular-dropdowns':{
			deps: ['angular']
		},
		'facebook': {
			exports: 'FB'
		},
		'google':{
			exports:'gapi'
		},
		'tinymce': {
			deps: ['angular']
		},
		'ui-tinymce': {
			deps: ['angular','tinymce']
		},
		'angular-local-storage': {
			deps: ['angular']
		},
		'angular-multirange-slider': {
			deps: ['angular']
		}
	},
	priority: [
		"angular"
	]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = "NG_DEFER_BOOTSTRAP!";

require( [
	'angular',
	'app',
	'routes',
	'fb',
	'google'
], function(angular, app, routes, bootstrap) {

	var $html = angular.element(document.getElementsByTagName('html')[0]);
    function loadCSS(url){
        var css = document.createElement('link');
        css.rel = 'stylesheet';
        css.href = url;
        document.head.appendChild(css);
    }
    loadCSS('../../bower_components/angucomplete/angucomplete.css');
    loadCSS('../../bower_components/ngImgCrop/compile/minified/ng-img-crop.css');
	loadCSS('../../bower_components/angular-loading-bar/build/loading-bar.css');
	loadCSS('../../bower_components/angular-dropdowns/angular-dropdowns.css');
	loadCSS('../../bower_components/angular-multirange-slider/dist/multirange-slider.css');
	loadCSS('app/css/animate.css');

	angular.element().ready(function() {
		angular.resumeBootstrap([app['name']]);
		var elements = document.getElementsByClassName('displayNone');
		for (var i = 0; i < elements.length;  elements = document.getElementsByClassName('displayNone')){
			var nClass = elements[i].className.replace(/\bdisplayNone\b/,'');
			elements[i].className = nClass;
		}
	});


});
