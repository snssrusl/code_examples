var stackForStates = {
    initJson: '',
    states: [],
    canvas: '',
    countOfUse: 0,
    initialize: function(canvas){
        this.canvas = canvas;
        this.initJson = this.canvas.toJSON(['baseScale', 'sepia', 'grayscale', 'horizontal', 'vertical']);
        this.initJson.horizontal = this.canvas.horizontal;
        this.initJson.vertical = this.canvas.vertical;
        //console.log(this.initJson);
    },
    push: function(){
        if (this.canvas !== '') {
            var canvasObjects = this.canvas.toJSON(['baseScale', 'sepia', 'grayscale','horizontal', 'vertical']);
            canvasObjects.horizontal = this.canvas.horizontal;
            canvasObjects.vertical = this.canvas.vertical;
            var json = JSON.stringify(canvasObjects);
            var use = this.checkUse();
            if ($("#undo").length) {
                if ($("#undo").hasClass("disableundo") && use) {
                    $("#undo").removeClass("disableundo");
                }
            }
            this.states.push(json);
        }
    },
    pop: function(){
        var el = this.states.pop();
        if ($("#undo").length && this.states.length == 0){
            $("#undo").addClass("disableundo");
        }
        return el;
    },
    checkUse: function(){
        ++this.countOfUse;
        if (this.countOfUse > 1){
            return true;
        }
        return false;
    },
    consoleInfo: function(){
        //console.log(this.states);
    }
};

$(function(){

    if ( window.opener != null){
        var hash = window.location.hash;
        if(window.location.search.indexOf('fromfacebook') == -1){
            window.close();

            hash = hash.replace("#access_token=","");
            var date = new Date( new Date().getTime() + 60*60*1000 );
            window.opener.document.cookie="instToken="+hash+";path=/; expires="+date.toUTCString();
            $('#inst_photos', window.opener.document).trigger("click");
        }
    }
    var canvas = new fabric.Canvas('product_designer_canvas');
    canvas.horizontal = false;
    canvas.vertical = true;
    fabric.Image.filters.Sepia3 = fabric.util.createClass(fabric.Image.filters.BaseFilter, /** @lends fabric.Image.filters.Sepia3.prototype */ {

        /**
         * Filter type
         * @param {String} type
         * @default
         */
        type: 'Sepia3',

        /**
         * Applies filter to canvas element
         * @memberOf fabric.Image.filters.Sepia.prototype
         * @param {Object} canvasEl Canvas element to apply filter to
         */
        applyTo: function(canvasEl) {
            var context = canvasEl.getContext('2d'),
                imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
                data = imageData.data,
                iLen = data.length, i, r, g, b;

            for (i = 0; i < iLen; i+=4) {
                r = data[i];
                g = data[i + 1];
                b = data[i + 2];

                data[i] = (r * 0.393 + g * 0.769 + b * 0.189 );
                data[i + 1] = (r * 0.349 + g * 0.686 + b * 0.168 );
                data[i + 2] = (r * 0.272 + g * 0.534 + b * 0.131 );
            }

            context.putImageData(imageData, 0, 0);
        }
    });
    fabric.Image.filters.Sepia3.fromObject = function() {
        return new fabric.Image.filters.Sepia3();
    };
    canvas.undo = false;
    canvas.modified = false;
    canvas.scale = false;
    canvas.backgroundColorChange = false;
    //stackForStates.initialize(canvas);
    var getQuery = window.location.search.substring(1);
    var vars = getQuery.split('&');
    var quantityVars = vars.length;
    var pairs = [];
    var canvasJson;
    var canvasObjectSelect = false;
    function callBackForRender(){
        canvas.renderAll.bind(canvas);
        $('#upload-image-loader').addClass("displayNone");
    }
    $("#undo").click(function(){
        if (!$("#undo").hasClass("disableundo")) {
            $('#upload-image-loader').removeClass("displayNone");
            stackForStates.consoleInfo();
            if (canvas.modified)
                stackForStates.pop();
            var json = stackForStates.pop();
            //console.log(json);
            canvas.undo = true;
            canvas.backgroundColor = null;
            //canvas.backgroundImage = null;

            canvas.loadFromJSON(json, function(){canvas.renderAll.bind(canvas), $('#upload-image-loader').addClass("displayNone");});
            var object = JSON.parse(json);
            canvas.vertical = object.vertical;
            canvas.horizontal = object.horizontal;
            canvas.undo = false;
            canvas.modified = false;
            setTextColorPicker('#000000');
            if ($('select[name="colorpicker-picker-longlist-back"]').simplecolorpicker != undefined) {
                $('select[name="colorpicker-picker-longlist-back"]').simplecolorpicker('selectColor', '#FFFFFF');
            }
            if (object.horizontal) {
                $(".vetical_view").removeClass("sel");
                $(".horizontal_view").addClass("sel");
            }
            if (canvas.vertical) {
                $(".vetical_view").addClass("sel");
                $(".horizontal_view").removeClass("sel");
            }
        }
    });
    for (var i = 0; i < quantityVars; i++){
        pairs[i] = vars[i].split('=');
        if (pairs[i][0] != undefined && pairs[i][0] === 'save_later'){
            var id = pairs[i][1];
            $.ajax({
                type: 'GET',
                url: '/customize',
                dataType: 'json',
                async: false,
                data: { 'save_id': id, 'get_save_later':true},
                success: function(result) {
                    if(result.status == 0){
                        alert("Error loading save case. Please check you link for saving case");
                    } else if (result.status == 1){
                        if(result.json){
                            canvasJson = result.json[0].jsondata;
                        }
                    }
                }

            });
        }
        if (pairs[i][0] != undefined && pairs[i][0] === 'onselect'){
            canvasObjectSelect = getCookie('canvasSelectChange');
            window.canvasObjectSelect = canvasObjectSelect;
            var canvasBackgroundColor = getCookie('canvasBackgroundColor');
        }
        if (pairs[i][0] != undefined && pairs[i][0] === 'framechange'){
            canvas.framechangecookie = getCookie('canvasFrameChange');
        }
    }
    //initialize product-designer canvas
    //var canvas = new fabric.Canvas('product_designer_canvas');
    canvas.centeredRotation = true;
    document.getElementById('product_designer_canvas').fabric = canvas;

    canvas.setHeight(600);
    canvas.setWidth(600);
    canvas.selection = false;
    var renderCanvasForBackground = function (canvas) {
        stackForStates.initialize(canvas);
        canvas.renderAll.bind(canvas);
    }
    if(canvasJson != undefined){
        canvas.loadFromJSON(canvasJson, renderCanvasForBackground(canvas));
    } else{
        var canvasStore = getCookie('canvasStore');
        if(typeof canvasStore != 'undefined'){
            var objects = JSON.parse(canvasStore);
            canvas.loadFromJSON(canvasStore, renderCanvasForBackground(canvas));
        }else{
            var overlayUrl = $('#forOverlay').data('overlay-url');
            var designerImageUrl = $('#forOverlay').data('background-url');
            if($("#thisCustomizeType").length && $("#thisCustomizeType").data("customize-type") == 'engraved' && $("#thisCustomizeType").data("type")=="plate"){
                //$('span:contains("Silver/Black")').parent().trigger("click");
                designerImageUrl = $('span:contains("Silver/Black")').parent().data("image");
                //console.log("Silver/Black");
            }
            if (canvasBackgroundColor != undefined && canvasBackgroundColor != null){
                canvas.backgroundColor = canvasBackgroundColor;
            }else {
                var imgBackground = fabric.Image.fromURL(designerImageUrl, function (oImg) {
                    var height = oImg.getHeight(),
                        width = oImg.getWidth();
                    var coeff = 1;
                    if (width > 600) {
                        coeff = 1 / (width / 600);
                    }

                    scale = coeff;
                    oImg.scale(scale);
                    canvas.setBackgroundImage(oImg, canvas.renderAll.bind(canvas));
                });
            }
            if($('#forOverlay').attr('frameFlag') == undefined){
            var imgOverlay = fabric.Image.fromURL(overlayUrl, function(oImg) {
                var height = oImg.getHeight(),
                    width = oImg.getWidth();
                var coeff = 1;
                if (width > 600){
                    coeff = 1/ (width / 600);
                }

                scale = coeff;
                oImg.scale(scale);
                canvas.setOverlayImage(oImg,renderCanvasForBackground(canvas));
                if (canvasObjectSelect != undefined){

                    var canvasObject =  canvas.toJSON();
                    //console.log(canvasObject);
                    var objects = JSON.parse(window.canvasObjectSelect);
                    canvasObject.objects = objects;
                    //console.log(canvasObject);
                    canvas.loadFromJSON(JSON.stringify(canvasObject), function(obj){
                        canvas.forEachObject(function(obj){
                            if (obj.active){
                                canvas.setActiveObject(obj);
                            }
                            if (obj.sepia){
                                obj.filters.push(new fabric.Image.filters.Sepia3());
                                obj.applyFilters(canvas.renderAll.bind(canvas));
                            }
                            if (obj.grayscale){
                                obj.filters.push(new fabric.Image.filters.Grayscale());
                                obj.applyFilters(canvas.renderAll.bind(canvas));
                            }
                        });
                    });
                    canvas.renderAll();
                }

            });
            canvas.renderAll();
            }
            //}
        }
    }
    canvas.on({
        'object:modified': function(e){
            var object = e.target;
            if (object != undefined){
                if (canvas.scale){
                    var scaleX = e.target.scaleX;
                    var scaleY = e.target.scaleY;
                    scale = (scaleX > scaleY)? scaleX:scaleY;
                    var baseScale = e.target.baseScale;
                    var left = e.target.getLeft();
                    if (scale > 10){
                        e.target.scaleX = 10;
                        e.target.scaleY = 10;
                        e.target.left = left;
                        $(".point").css({"left": '100px'});
                        return;
                    }
                    //console.log('Scale: ' + scale);
                    setScale(scale);
                }
            }


            if (!canvas.undo)
                stackForStates.push();
            canvas.modified = true;
            canvas.scale = false;
            //console.log('Modified' + object);
        },
        'object:added': function(e){
            var object = e.target;
            /*if(!(object instanceof fabric.Image)) {
                if (!canvas.undo)
                    stackForStates.push();
                canvas.modified = true;
            }*/
            //console.log('Added' + object);
        },
        'object:removed': function(e){
            var object = e.target;
            stackForStates.push();
            canvas.modified = true;
            //console.log('Removed' + object);
        },
        'selection:cleared':function(){
            $('.knob').val(0).trigger("change");
            //$(".point").css({"left": '50px'});
            $("#slider11")
                .slider({ value: 50 })
            $("#text_area").val("");
            $("#left_alignment > img").trigger("click");
            $('#text_area').css({'text-align':'left'});
            setTextColorPicker('#000000');
            setBackgroundColorPicker('#FFFFF');
            $("#transparent_check").prop("checked", true);
            if($("#original_effect").css("display") != "block") {
                $(".effects_list > a > #ef2").trigger("click");
            }
            if($("#top_line_text").length){
                $("#top_line_text").val('');
                $("#bottom_line_text").val('');
            }

        },
        'object:scaling': function(e){
            canvas.scale = true;
        },
        'object:rotating' : function(e){
            var angle = e.target.getAngle();
            while (angle>360)
                angle-=360;
            angle = parseFloat(angle)/360*100;
            canvas.rotateControl = true;
            $('.knob').val(angle).trigger('change');
        },
        'object:selected': function(e){
            $("#text_area").val("");
            var object = e.target;
            //console.log(object.stateProperties);
            var angle = object.getAngle();
            while(angle >= 360)
                angle-=360;
            angle = parseFloat(angle)/360*100;
            $(".knob").val(angle).trigger("change");
            var scale = object.getScaleX();
            var baseScale = object.baseScale;
            var realScale = scale/baseScale;
            setScale(realScale);
            if (object instanceof fabric.Text){
                var text = object.getText();
                if($("#top_line_text").length){
                    if(object.topText != undefined && object.topText){
                        text = (text === 'Top line')? '':text;
                        $("#top_line_text").val(text);
                    }
                    if(object.bottomText != undefined && object.bottomText){
                        text = (text === 'Bottom line')? '':text;
                        $("#bottom_line_text").val(text);
                    }
                    if (text === ''){
                        object.setText('');
                    }
                }

                var textAlign = object.getTextAlign();
                $("#" + textAlign +"_alignment > img").trigger("click");
                var data = $("#text_area").val();
                var textColor = object.getFill();
                var backgroundColor = object.getTextBackgroundColor();
                if(backgroundColor){
                    $("#transparent_check").prop("checked", false);
                    setBackgroundColorPicker(backgroundColor);
                } else {
                    $("#transparent_check").prop("checked", true);
                    setBackgroundColorPicker('#FFFFF');
                }
                setTextColorPicker(textColor);

                $("#text_area").focus().val('').val(text);
            } else {
                if (typeof object.sepia != 'undefined' && object.sepia){
                    $('.effects_list > a > #ef3').trigger("click");
                }
                else if (typeof object.grayscale != 'undefined' && object.grayscale){
                    $('.effects_list > a > #ef1').trigger("click");
                }
            }
        }

    });

    var changeTextColor = function(){
        /*var textColor = document.getElementById('text_color_input').value;
        var object = canvas.getActiveObject();
        if (object instanceof  fabric.Text){
            object.setFill('#'+textColor);
            canvas.renderAll();
        }*/
    };
    var changeBackgroundColor = function() {
        if(!$("#transparent_check").prop("checked")){
            var backgroundColor = document.getElementById('back_color_input').value;
            var object = canvas.getActiveObject();
            if (object instanceof  fabric.Text){
                object.setTextBackgroundColor('#'+backgroundColor);
                canvas.renderAll();
            }
        }
    };

    document.getElementById('text_color_input').onchange = changeTextColor;
    document.getElementById('back_color_input').onchange = changeBackgroundColor;

    $(".horizontal_view").click(function(){
        if(!$(this).hasClass("sel")){
            $('#upload-image-loader').removeClass("displayNone");
            stackForStates.push();
            canvas.undo = true;
            var rotatedOverlayUrl = $('#forOverlay').data('rotated-overlay-url');
            var rotatedBackgroundUrl = $('#forOverlay').data('rotated-background-url');

            var left_canvas = 205,
                top_canvas = 105;
            //var canvas = document.getElementById('product_designer_canvas').fabric;
            var objectActive = canvas.getActiveObject();
            var canvasObjects = canvas.toJSON(['baseScale', 'sepia', 'grayscale', 'active']);

            //canvasObjects.backgroundImage.src = rotatedBackgroundUrl;

            //canvasObjects.overlayImage.src = rotatedOverlayUrl;

            var objects = canvasObjects.objects;
            var objectsQuantity = objects.length;
            for (var i = 0; i < objectsQuantity; i++){
                var obj = objects[i];
                var object_left = obj.left,
                    object_top = obj.top;
                var offset_left = object_left - left_canvas,
                    offset_top = object_top - top_canvas;
                obj.angle = obj.angle + 90;
                obj.left = 105+393-offset_top;
                obj.top =  205+offset_left;
                if (obj.angle > 360)
                    obj.angle -=360;
                obj.filters = [];
            }
            canvas.loadFromJSON(JSON.stringify(canvasObjects), function(obj){
                if (canvas.backgroundImage != null){
                    var imgBackground = fabric.Image.fromURL(rotatedBackgroundUrl, function(oImg) {
                        var height = oImg.getHeight(),
                            width = oImg.getWidth();
                        var coeff = 1;
                        if (width > 600){
                            coeff = 1/ (width / 600);
                        }
                        scale = coeff;
                        oImg.scale(scale);
                        canvas.setBackgroundImage(oImg,canvas.renderAll.bind(canvas));
                    });
                }
                //canvasObjects.overlayImage.src = rotatedOverlayUrl;

                var imgOverlay = fabric.Image.fromURL(rotatedOverlayUrl, function(oImg) {
                    var height = oImg.getHeight(),
                        width = oImg.getWidth();
                    var coeff = 1;
                    if (width > 600){
                        coeff = 1/ (width / 600);
                    }

                    scale = coeff;
                    oImg.scale(scale);
                    canvas.setOverlayImage(oImg,canvas.renderAll.bind(canvas));
                    $("#upload-image-loader").addClass("displayNone");
                });
                canvas.forEachObject(function(obj){
                    if (obj.active){
                        canvas.setActiveObject(obj);
                    }
                    if (obj.sepia){
                        obj.filters.push(new fabric.Image.filters.Sepia3());
                        obj.applyFilters(canvas.renderAll.bind(canvas));
                    }
                    if (obj.grayscale){
                        obj.filters.push(new fabric.Image.filters.Grayscale());
                        obj.applyFilters(canvas.renderAll.bind(canvas));
                    }
                });


            });
            canvas.horizontal = true;
            canvas.vertical = false;
            canvas.renderAll();
            $(this).addClass("sel");
            $(".vetical_view").removeClass("sel");
            canvas.undo = false;
            /*stackForStates.pop();
            stackForStates.pop();*/
        }
    });

    $(".vetical_view").click(function(){
        if(!$(this).hasClass("sel")){
            $('#upload-image-loader').removeClass("displayNone");
            stackForStates.push();
            canvas.undo = true;
            var right_canvas = 105+393,
                top_canvas = 205;

            var overlayUrl = $('#forOverlay').data('overlay-url');
            var designerImageUrl = $('#forOverlay').data('background-url');
            var canvasObjects = canvas.toJSON(['baseScale', 'sepia', 'grayscale', 'active']);
            //canvasObjects.backgroundImage.src = designerImageUrl;
            //canvasObjects.overlayImage.src = overlayUrl;

            var objects = canvasObjects.objects;
            var objectsQuantity = objects.length;
            for (var i = 0; i < objectsQuantity; i++){
                var obj = objects[i];
                var object_left = obj.left,
                    object_top = obj.top;
                var offset_left = right_canvas - object_left,
                    offset_top = object_top - top_canvas;
                obj.angle = obj.angle - 90;
                obj.left = 205+offset_top;
                obj.top =  105+offset_left;
                if (obj.angle < 0)
                    obj.angle +=360;
                obj.filters = [];
            }
            canvas.loadFromJSON(JSON.stringify(canvasObjects), function(obj){
                if (canvas.backgroundImage != null){
                    var imgBackground = fabric.Image.fromURL(designerImageUrl, function(oImg) {
                        var height = oImg.getHeight(),
                            width = oImg.getWidth();
                        var coeff = 1;
                        if (width > 600){
                            coeff = 1/ (width / 600);
                        }

                        scale = coeff;
                        oImg.scale(scale);
                        canvas.setBackgroundImage(oImg,canvas.renderAll.bind(canvas));
                    });
                }

                var imgOverlay = fabric.Image.fromURL(overlayUrl, function(oImg) {
                    var height = oImg.getHeight(),
                        width = oImg.getWidth();
                    var coeff = 1;
                    if (width > 600){
                        coeff = 1/ (width / 600);
                    }

                    scale = coeff;
                    oImg.scale(scale);
                    canvas.setOverlayImage(oImg,canvas.renderAll.bind(canvas));
                    $("#upload-image-loader").addClass("displayNone");
                });
                canvas.forEachObject(function(obj){
                    if (obj.active){
                        canvas.setActiveObject(obj);
                    }
                    if (obj.sepia){
                        obj.filters.push(new fabric.Image.filters.Sepia3());
                        obj.applyFilters(canvas.renderAll.bind(canvas));
                    }
                    if (obj.grayscale){
                        obj.filters.push(new fabric.Image.filters.Grayscale());
                        obj.applyFilters(canvas.renderAll.bind(canvas));
                    }
                });

            });
            canvas.horizontal = false;
            canvas.vertical = true;
            canvas.renderAll();
            $(this).addClass("sel");
            $(".horizontal_view").removeClass("sel");
            /*stackForStates.pop();
            stackForStates.pop();*/
            canvas.undo = false;
        }
    });

    function roundRect(ctx,x, y, w, h, radius)
    {
        var r = x + w;
        var b = y + h;
        ctx.beginPath();
        ctx.strokeStyle="gray";
        ctx.lineWidth="2";
        ctx.moveTo(x+radius, y);
        ctx.lineTo(r-radius, y);
        ctx.quadraticCurveTo(r, y, r, y+radius);
        ctx.lineTo(r, y+h-radius);
        ctx.quadraticCurveTo(r, b, r-radius, b);
        ctx.lineTo(x+radius, b);
        ctx.quadraticCurveTo(x, b, x, b-radius);
        ctx.lineTo(x, y+radius);
        ctx.quadraticCurveTo(x, y, x+radius, y);
        ctx.stroke();
    }

    function scaleActiveObject(left){
        var object = canvas.getActiveObject();

        if(typeof object != 'undefined' && object != null){
            var baseScale = object.baseScale;
            var scale = 0;
            if (left >=50){
                scale = (left-50)/5 + 1;
                scale = (scale > 10)?10 : scale;
                scale = (left==50)?1:scale;
            } else {
                scale = 0.5 + left/100;
            }
            object.scale(baseScale*scale);
            canvas.renderAll();
            //canvas.fire("object:modified");
        }
    }
    var flag = false;
    var offset = $('.point1').offset().left;
    var offsetL = $('.line').offset().left;
    var pos = 0;
    var pos1 = 0,pos2 = 0;
    $('.plus').click(function(){
        if(parseInt($('.point1').css('left')) < 99){
            $('.point1').css('left','+=2');
            var x = (parseInt($('.point1').css('left')));
            scaleActiveObject(x);
            if(canvas.getActiveObject() != undefined)
                canvas.fire("object:modified");
        }

    });
    $('.minus').click(function(){
        if(parseInt($('.point1').css('left')) > 1){
            $('.point1').css('left','-=2');
            var x = (parseInt($('.point1').css('left')));
            scaleActiveObject(x);
            if(canvas.getActiveObject() != undefined)
                canvas.fire("object:modified");
        }

    });
    $('.point').bind('mousedown touchstart',function(e){
        if(e.originalEvent.touches){
            var pos1 = e.originalEvent.touches[0].pageX||0;
        }
        pos = e.pageX||pos1 - offset;
        flag = true;
    });
    $('.line').bind("mousemove touchmove",function(e){
        var x = parseInt($('.point').css('left'));
        if(flag == true){
            if(e.originalEvent.touches){
                var vv = e.originalEvent.touches[0].pageX||parseInt($('.point1').css('left'));
            }
            pos2 = (typeof e.pageX != 'undefined')? e.pageX-offset: vv-offset;
            var pos0 = Math.abs(pos-pos2);
            if(pos > pos2){
                x-=pos0;
            }else{
                x+=pos0;
            }
            if(x > (-1) && x < 101){
                $('.point').css({
                    "left": x + "px"
                });
                scaleActiveObject(x);
                if(canvas.getActiveObject() != undefined)
                    canvas.fire("object:modified");
            }
            pos = pos2;
        }
    });
    $( "#slider11" ).slider({
        change: function( event, ui ) {
            if (!canvasObjectSelect){
                scaleActiveObject($("#slider11").slider( "option", "value" ));
    //            console.log($("#slider11").slider( "option", "value" ));
            }
            canvasObjectSelect = false;
        }
    });
//    $('.line').bind('mousedown touchstart',function(e){
//        if(e.originalEvent.touches){
//            var pos1 = e.originalEvent.touches[0].pageX||0;
//        }
//        pos2 = (typeof e.pageX != 'undefined')? e.pageX-offsetL: vv-offsetL;
//        if((pos2-5) > (-1) && pos2 < 110){
//            pos2 -= 5;
//        }else{
//            if(pos2 > 105) pos2 = 100;
//        }
//        $('.point1').css({
//            "left": pos2 + "px"
//        });
//        scaleActiveObject(pos2);
//
//    });
    $("#slider11")
        .slider({ value: 50 })
        .draggable();
    $("#reset_controls").click(function(){
        //$(".point").css({"left":50});
        $(".knob").val(0).trigger("change");
        $("#slider11")
            .slider({ value: 50 });
        var object = canvas.getActiveObject();
        if(typeof object != 'undefined' && object != null){
            object.setAngle(0);
            object.scale(object.baseScale);
            canvas.renderAll();
        }
    });
    $(document).bind('mouseup touchend',function(){
        flag = false;
    });

    function setScale(scale)
    {
        //console.log(scale);
        if(scale > 10 || scale < 0.5)
            return;
        if (scale > 1){
            scale = 50*((scale-1)/9);
        } else if (scale == 1){
            scale = 0;
        }
        else {
            scale = scale - 0.5;
            scale = (scale *100)-50;
        }
        var left = 50 + scale;
        //$(".point").css({"left": left+ 'px'});
        canvasObjectSelect = true;
        $("#slider11")
            .slider({ value: left });
    }
    $("#share").click(function(event){
        var pathName = window.location.pathname;
        var params = pathName + window.location.search;
        var indexOfSaveForLater = params.indexOf("save_later");
        params = (indexOfSaveForLater != -1)?  params.slice(0,indexOfSaveForLater-1) : params;
        var canvasObjects = canvas.toJSON(['baseScale', 'sepia', 'grayscale', 'bottomText', 'topText', 'backImage']);
        var json = JSON.stringify(canvasObjects);

        $.ajax({
            type: 'POST',
            url: '/customize',
            dataType: 'json',
            async: false,
            data: { 'params': params, 'share':true, 'json':json},
            success: function(result) {
                if(result != null){
                    if(result.status == 0){
                        alert("Please sign up for save your case");
                        return;
                    }
//                    var url = window.location.origin;
                    var  url = window.location.origin + result.params+"&fromfacebook=1";
                    params = document.location.origin + params;
                    //console.log(params);
                    var img = window.location.origin + createJpegFromCanvas();

                    //console.log(img);
//                    url = url.replace("?", "&");
                    params = encodeURIComponent(url + '&img='+img);
                    //console.log(params);
                    //console.log(url);
                    //console.log(url + '&img='+img);

                    //console.log(img);
                    //console.log(params +'&url='+ url + '&img='+img);
//                    url = url.replace("?", "&");
                    params = encodeURIComponent(params +'&url='+ url + '&img='+img);
                    //console.log(params);
                    //console.log(url);


                    //window.open('http://www.facebook.com/sharer.php?s=100&p[url]='+params,'sharer','toolbar=0,status=0,width=700,height=400');
                    FB.ui(
                        {
                            method: 'feed',
                            name: 'GetExclusive',
                            caption: 'Bringing Facebook to the desktop and mobile web',
                            description: (
                                'The best site for license plate!'
                                ),
                            link:url,
                            picture:img
                        },
                        function(response) {});
                }else{
                    alert("Please sign up for save your case");
                    url = false;
                }
                if(result.status == 0){

                } else if (result.status == 1){
                    //alert("Please check your email for link");
                }
            }
        });
    });
    $("#save_for_later").click(function(){
            var canvasObjects = canvas.toJSON(['baseScale', 'sepia', 'grayscale', 'bottomText', 'topText', 'backImage']);
            var pathName = window.location.pathname;
            var params = window.location.hostname + pathName + window.location.search;
            var indexOfSaveForLater = params.indexOf("save_later");
            params = (indexOfSaveForLater != -1)?  params.slice(0,indexOfSaveForLater-1) : params;
            var json = JSON.stringify(canvasObjects);

            //console.log('params'+ params+ ';json'+json);
            $.ajax({
                type: 'POST',
                url: '/customize',
                dataType: 'json',
                async: false,
                data: { 'params': params, 'check_logged':true, 'json':json },
                success: function(result) {
                    //console.log(result);
                    if(result.status == 0){
                        alert("Please sign up for save your case");
                    } else if (result.status == 1){
                        alert("Please check your email for link");
                    }
                }
            });
        var canvasObjects = canvas.toJSON(['baseScale', 'sepia', 'grayscale']);
        /*console.log(canvasObjects);
        var json = JSON.stringify(canvasObjects);
        console.log(JSON.parse(json));
        var date = new Date( new Date().getTime() + 60*60*1000 );
        document.cookie="canvasStore="+json+";path=/; expires="+date.toUTCString();*/
    });



    function setTextColorPicker(color) {

        /*if(document.getElementById('text_color_input').color != undefined){
            document.getElementById('text_color_input').color.fromString(color);
            $("#text_color > div").css('background-color', color);
        }*/
        if ($('select[name="colorpicker-picker-longlist"]').simplecolorpicker != undefined) {
            $('select[name="colorpicker-picker-longlist"]').simplecolorpicker('selectColor', color);
        }
    }

    function setBackgroundColorPicker(color) {
        if (document.getElementById('back_color_input').color != undefined){
            document.getElementById('back_color_input').color.fromString(color);
            $("#back_color > div").css('background-color', color);
        }
    }



    var optLargeView = {
        autoOpen: false,
        height: 750,
        width: 750,
        modal: true,
        buttons: {
            "Close": function(){
                $('#large_view_dialog .content_view').empty();
                $( this ).dialog( "close" );
            }
        },
        close: function() {
            $('#large_view_dialog .content_view').empty();
        }
    };

    $("#largeViewBtn").click(function(){
        $('#large_view_dialog .content_view').append('<img src="' + createJpegFromCanvas() + '" style="top:0; left: 0; position:absolute;"  >');
        $("#large_view_dialog").dialog(optLargeView).dialog("open").dialog({
            resizable: false
        });
    });
    function createJpegFromCanvas(){
        var imageSrc = '';
        var activeObject = canvas.getActiveObject();
        if (activeObject != undefined){
            canvas.deactivateAll();
            canvas.renderAll();
        }
        var canvasImage = canvas.toDataURL('jpeg');
        $.ajax({
            type: 'POST',
            url: '/customize',
            dataType: 'json',
            async: false,
            data: {image: canvas.toDataURL('png'),save_canvas_image:true },
            success: function(result) {
                imageSrc = result.src;

            }
        });
        if(activeObject != undefined)
            canvas.setActiveObject(activeObject);
        return imageSrc;
    }

    $("#add_to_cart_designer").click(function(){
        var imagePath = createJpegFromCanvas();
        $('#add_to_cart_designer_dialog .content_view').html('<img src="' + imagePath + '" style="top:0; left: 0; position:absolute;"  >');
        var form = $('#buy_block');
        form.find('input#image').val(imagePath);
        var canvasObjects = canvas.toJSON(['baseScale', 'sepia', 'grayscale']);
        var json = JSON.stringify(canvasObjects);
        form.find('input#jsonData').val(json);
        $("#add_to_cart_designer_dialog").dialog("open");
    });

    $("#add_to_cart_designer_dialog").dialog({
        autoOpen: false,
        height: 750,
        width: 750,
        modal: true,
        resizable: false,
        buttons: {
            "Approve": function() {
                document.getElementById('buy_block').submit();
            },
            Back: function() {
                $( this ).dialog( "close" );
            }
        },
        close: function() {
        }
    });

    $('#inst_photos').click(function(){
        var elementId = $(this).attr("id");
        var contentBlock = '#' + $(this).attr("data-content-block");
        var dialogId = '#'+elementId+"_dialog";

        var instToken = getCookie('instToken');
        if (typeof instToken != 'undefined') {
            url = window.socialservice_uri + '/instagram/photo/get?access_token='+instToken;
            $.ajax({
                url: '/ajax.php',
                data: {getInstPhotos : 'getInstPhotos', instToken: instToken},
                dataType: 'json',
                modal: true,
                beforeSend: function(){
                    var imgLoading = "<img id=\"loader_img\" height=\"24\" width=\"24\" src=\"/img/loader.gif\" />";
                    $(contentBlock).html(imgLoading);
                },
                success: function(message){
                    if (message){
                        $(contentBlock).html(message);
                    } else {
                        $(contentBlock).html("<p>Your album is empty</p>");
                    }
                    $("#instagram_content").customScrollbar({
                        skin: "default-skin",
                        hScroll: true,
                        updateOnWindowResize: true
                    });
                    $("#facebook_content").customScrollbar({
                        skin: "default-skin",
                        hScroll: true,
                        updateOnWindowResize: true
                    });
                }
            }).fail(function(){
                    $(contentBlock).html('Error get message');
                });
            $(dialogId).attr("data-block-content", $(this).attr("data-content-block"));
            $(dialogId).dialog("open");
        }else {
            var hostname = window.location.hostname;
            var clientId = '';
            var redirect_uri = '';
            switch (hostname){
                case 'www.phonecase.loc':
                case 'phonecase.loc':
                    clientId = '6db280c61ddd4d08a19d0daeab51a36a'
                    redirect_uri = 'http://phonecase.loc/customize'
                    break;
                case 'www.phonecase.testserver.kiev.ua':
                case 'phonecase.testserver.kiev.ua':
                    clientId = '9d3399e2c3fd43578cef08a057783c15'
                    redirect_uri = 'https://phonecase.testserver.kiev.ua/customize'
                    break;
                case 'test.getexclusive.com':
                case 'www.test.getexclusive.com':
                    clientId = '1d859190fd6849b7a23289c50be91b74'
                    redirect_uri = 'https://test.getexclusive.com/customize'
                    break;
                default:
                    clientId = '7248e1fb7c814496b0ac6fb3854caa65'
                    redirect_uri = 'https://phonecase.test.5-soft.com/customize'
            }
            var url = 'https://instagram.com/oauth/authorize/?client_id='+clientId+'&redirect_uri='+redirect_uri+'&response_type=token';
            var windowParameters = "height=400,width=500, top=100, left=100";
            var win = window.open(url, "Instagram Auth", windowParameters);
        }
    });

    $('#fbk_photos').click(function(){
        var elementId = $(this).attr("id");
        var contentBlock = '#' + $(this).attr("data-content-block");
        var dialogId = '#'+elementId+"_dialog";
        var fbkToken = getCookie('fbkToken');
        if(typeof fbkToken != 'undefined'){
            $.ajax({
                url: '/ajax.php',
                data: {getFbkPhotos : 'getFbkPhotos', fbkToken: fbkToken},
                dataType: 'json',
                modal: true,
                beforeSend: function(){
                    var imgLoading = "<img id=\"loader_img\" width=\"24\" height=\"24\" src=\"/img/loader.gif\" />";
                    $(contentBlock).html(imgLoading);
                },
                complete: function(){

                },
                success: function(message){
                    if (message){
                        $(contentBlock).html(message);
                    } else {
                        $(contentBlock).html("<p>Your album is empty</p>");
                    }
                    $("#instagram_content").customScrollbar({
                        skin: "default-skin",
                        hScroll: true,
                        updateOnWindowResize: true
                    });
                    $("#facebook_content").customScrollbar({
                        skin: "default-skin",
                        hScroll: true,
                        updateOnWindowResize: true
                    });
                }
            }).fail(function(){
                    $(contentBlock).html('Error get message');
                });
            $(dialogId).attr("data-block-content", $(this).attr("data-content-block"));
            $(dialogId).dialog("open");
            //        $('#inst_photos_dialog').dialog("open");
        } else {
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token
                    // and signed request each expire
                    var accessToken = response.authResponse.accessToken;
                    setFbkToken(accessToken);
                } else if (response.status === 'not_authorized') {
                    FB.login(function(respons){
                        if(respons.authResponse){
                            var accessToken = respons.authResponse.accessToken;
                            setFbkToken(accessToken);
                        }
                    }, {scope: 'email,user_likes, user_photos'});
                    // the user is logged in to Facebook,
                    // but has not authenticated your app
                } else {
                    FB.login(function(respons){
                        if(respons.authResponse){
                            var accessToken = respons.authResponse.accessToken;
                            setFbkToken(accessToken);
                        }
                    }, {scope: 'email,user_likes, user_photos'});
                    // the user isn't logged in to Facebook.
                }
            });
        }
    });

    function setFbkToken(accessToken){
        var date = new Date( new Date().getTime() + 60*60*1000 );
        document.cookie="fbkToken="+accessToken+";path=/; expires="+date.toUTCString();
        $('#fbk_photos').trigger("click");
    }

    $(document).ready(function (){
        //$(".canvas").css('position','absolute');
       // var canvas = document.getElementById('product_designer_canvas').fabric;
        $(".canvas").css('overflow','visible');
        $(".content").css('overflow','visible');
        $(".content_wrapper").css('overflow','visible');
        //$(".canvas-container").css('position','absolute');
        $("canvas").parents("*").css("overflow", "visible");
       // stackForStates.initialize(canvas);
    });

});