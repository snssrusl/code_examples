/**
 * Created by ron on 1/29/14.
 */
$(function(){

    var canvas = document.getElementById('product_designer_canvas').fabric;

    //function for adding img using img srf to fabric canvas
    function addImgToFabric(src){
        if (!canvas.undo)
            stackForStates.push();
        canvas.modified = true;
        fabric.Image.fromURL(src, function(oImg) {
            $(".effects_list > a > #ef2").trigger("click");
            var height = oImg.getHeight(),
                width = oImg.getWidth();
            scale = 1;
            oImg.scale(scale);
            oImg.set({left: 300, top: 250});
            oImg.originX = "center";
            oImg.originY = "center";
            oImg.baseScale = scale;
            oImg.minScaleLimit = 0.5*oImg.baseScale;
            canvas.add(oImg);
            canvas.setActiveObject(oImg);
            canvas.renderAll();
        });
    }

    function addText(){
        if (!canvas.undo)
            stackForStates.push();
        canvas.modified = true;
        var text =  $("#text_area").val().split('\n');
        var length = text.length;
        for(var i = 0; i < length; ++i){
            text[i] = $.trim(text[i]);
        }
        text = text.join('\n');
        var fontFamily = $("#font_select").val();
        if ($('.font_selector').length){
            fontFamily = $('.font_selector').find(".active").data("font");
        }
        var align = getAlignment();
        //var textColor ="#" + $("#text_color_input").val();
        var textColor = $('select[name="colorpicker-picker-longlist"]').val();
        if ($('.widget_body').find('.style_btn.active').length ){
            textColor = $('.widget_body').find('.style_btn.active').data("text");
        }
        if ($("#thisCustomizeType").data("type") != undefined && $("#thisCustomizeType").data("type") == "phoneplate"){
            textColor = "#996633";
        }
        //var canvas = document.getElementById('product_designer_canvas').fabric;
        var textObject   = new fabric.Text(text, {
            top        : 270,
            left       : 300,
            fontSize   : 28,
            backgroundColor : false,
            fill: textColor,
            fontFamily : fontFamily,
            centeredRotation: true,
            textAlign: align
        });

        textObject.originX = "center";
        textObject.originY = "center";
        textObject.baseScale = 1;
        textObject.minScaleLimit = 0.5*textObject.baseScale;
        canvas.add(textObject);
        var backgroundColor = "#"+$("#back_color_input").val();
        if(!$("#transparent_check").prop("checked"))
            textObject.setTextBackgroundColor(backgroundColor);
        if ($("#thisCustomizeType").data("type") != undefined && $("#thisCustomizeType").data("type") == "phoneplate"){
            textObject.setTextBackgroundColor(false);
        }
        canvas.setActiveObject(textObject);
        var data = $("#text_area").val();
        $("#text_area").focus().val('').val(data);
    }

    function fabricActiveObjectOperation(functionName){
        //var canvas = document.getElementById('product_designer_canvas').fabric;
        var object = canvas.getActiveObject();
        if (typeof object != 'undefined' && object != null){
            canvas[functionName](object);
        }
    }

    $('.font_selector div').on('click', function(event) {
        var font = $(event.target).data('font');

        var object = canvas.getActiveObject();
        if (object != undefined && object instanceof fabric.Text){
            object.fontFamily = font;
        }
        var element = $(event.target);
        canvas.renderAll().renderAll();

        if (element.hasClass('active')) {
            element.removeClass('active');
        } else {
            $('.font_selector div').removeClass('active');
            element.addClass('active');
        }
        canvas.renderAll().renderAll();
        stackForStates.push();
    });


    $( "#add_flag_dialog, #add_clippart_dialog, #inst_photos_dialog, #fbk_photos_dialog" ).dialog({
        autoOpen: false,
        height: 550,
        width: 620,
        modal: true,
        resizable: false,
        buttons: {
            "Ok": function() {
                var contentBlock = '#'+$("#"+this.id).attr("data-block-content");
                $( contentBlock +" .overview").children().each(function(index, element){
                    if($(element).hasClass('checked')){
                            var imgSrc = $(element).attr('src');

                            imgSrc = imgSrc.replace("https://","");
                            imgSrc = imgSrc.replace("http://","");
                            var imgSr = '';
                            if(contentBlock == "#facebook_content" || contentBlock == "#instagram_content"){
                                $.ajax({
                                    url: '/customize.php?download_photo',
                                    data: {src : imgSrc},
                                    dataType: 'json',
                                    async:false,
                                    success: function(message){
                                        imgSrc = message.src;

                                        addImgToFabric(imgSrc);

                                    }
                                }).fail(function(){
                                        $(contentBlock).html('Error get message');
                                    });
                            }else{
                                addImgToFabric(imgSrc);
                            }
                    }
                });
                $(this).dialog("close");
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        close: function() {
            var contentBlock = '#'+$("#"+this.id).attr("data-block-content");
            $(contentBlock +" .overview").empty();
        }
    });
    $( "#upload_image_dialog" ).dialog({
        autoOpen: false,
        height: 250,
        width: 450,
        modal: true,
        resizable: false,
        buttons: {
            /*"Ok": function() {
                *//*var imgId = $('#upload-image-preview').children().attr('id');
                var html = $('#upload-image-preview').html();
                $('#upload-image-preview').empty();

                $(".canvas").append(html);
                $("#"+imgId).css('display','none');
                var imgPath = $("#"+imgId).attr("src");
                addImgToFabric(imgPath);
                var imgElement = document.getElementById(imgId);*//*
                $(this).dialog("close");
            },*/
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        close: function() {
            $('#upload-image-preview').empty();
        }
    });


    $("#upload_image").click(function(){
       /* $("#upload_image_dialog").dialog("open");
        $("#upload_image_dialog").addClass("displayNone");
        $("#upload_image_dialog").css("display","none");*/
        $("#upload-image-view").trigger("click");
    });

    $("#clipCat").change(function(){
        $('#clippart_content').find('.overview').empty();
        var selectedCategory = $(this).children(":selected").attr("id");
        var engraved = $('#thisCustomizeType').data('customize-type');
        var url="customize.php?clipcat=1&cat="+selectedCategory+"&engraved="+engraved;
        $.ajax({
            url: url,
            dataType: 'json',
            success: function(message){
                $('#clippart_content').find('.overview').html(message.html);

            }
        }).fail(function(){
                $(contentBlock).html('Error get cliparts');
            });

    });
    $('div#upload_image_dialog>div:first-child').appendTo('span#ui-id-6');
    $("#add_flag, #add_clippart").click(function(){
        var engraved = $('#thisCustomizeType').data('customize-type');
        var elementId = $(this).attr("id");
        var url = "customize.php?"+elementId+"&engraved="+engraved;
        var contentBlock = '#' + $(this).attr("data-content-block");
        var dialogId = '#'+elementId+"_dialog";
        $.ajax({
            url: url,
            dataType: 'json',
            beforeSend: function(){
                $('.hideMe').show();
            },
            success: function(message){
                $('.hideMe').hide();
                $(contentBlock).html(message.html);
                $("#clippart_content").customScrollbar({
                      skin: "default-skin",
                      hScroll: true,
                      updateOnWindowResize: true
                  }); 
                $("#flag_content").customScrollbar({
                      skin: "default-skin",
                      hScroll: true,
                      updateOnWindowResize: true
                  });
            }
        }).fail(function(){
                $(contentBlock).html('Error get message');
        });
        $(dialogId).attr("data-block-content", $(this).attr("data-content-block"));
        $(dialogId).dialog("open");
    });

    $("#flag_content, #clippart_content, #instagram_content, #facebook_content").click(function(event){
        var elem = '#'+$(this).attr('id');
        var el = event.target;
        if(el.src != undefined){
            if($(el).hasClass('checked')){
                $(el).css("background-color", "");
                $(el).removeClass("checked");
            }else{
                $(elem+' .overview').children("img").each(function(index,element){
                    if($(element).hasClass('checked')){
                        $(element).css("background-color", "");
                        $(element).removeClass("checked");
                    }
                });
                $(el).css("background-color", "#ffe200");
                $(el).addClass("checked");
            }
        }else{
                $(elem+' .overview').children("img").each(function(index,element){
                    if($(element).hasClass('checked')){
                        $(element).css("background-color", "");
                        $(element).removeClass("checked");
                    }
                });

            }

    });
    var  loader = $('.hideMe');
    $('#upload-image-view').fileupload({
        url: '/customize.php?upload_image',
        dataType: 'json',
        start: function (e, data){
            $('#upload-image-loader').removeClass("displayNone");
        },
        stop: function (e, data) {
        },
        progressall: function (e, data) {
            $('.hideMe').show();
        },
        done: function (e, data) {
            $('.hideMe').hide();
            if(data.result.status=='ok'){
                $('#upload-image-loader').addClass("displayNone");
                $('#upload-image-preview').html(data.result.img);
                var imgId = $('#upload-image-preview').children().attr('id');
                var html = $('#upload-image-preview').html();
                $('#upload-image-preview').empty();
                //$(this).dialog("close");
                $(".canvas").append(html);
                $("#"+imgId).css('display','none');
                var imgPath = $("#"+imgId).attr("src");
                addImgToFabric(imgPath);
                var imgElement = document.getElementById(imgId);
                $( "#upload_image_dialog" ).dialog("close");
            }
            if(data.result.status=='error') {
                $( "#upload_image_dialog" ).dialog("open");
                $('#upload-image-preview').html('Something wrong ' + data.result.message);
            }
            if(data.result.status=='uploaderror') {
                $( "#upload_image_dialog" ).dialog("open");
                $('#upload-image-preview').html('Something wrong with upload');
            }
        }
    });

    $("#add_text").click(function(){
        var object = canvas.getActiveObject();
        if(object != undefined && object instanceof fabric.Text){
            canvas.deactivateAll();
            canvas.renderAll();
            $("#text_area").val("");
        }
        addText();
    });

    $("#bring_forward, #send_backward, #DelBtn").click(function(){
        if (canvas.framedesign == undefined){
            var functionName = $(this).attr('data-function');
            if (typeof functionName != 'undefined')
                fabricActiveObjectOperation(functionName);
        }
    });

    function setAlignmentForText()
    {

    }
    $(".Alignment").click(function(event){
        var id ='#' + event.target.id;
        var align = $(event.target).parent().attr("data-align");
        if(typeof align == 'undefined')
            return;
        var text = $("#text_area").val();
        applyAlignment(align,text,"#text_area",event);

    });


    function applyAlignment(align, text, element, event)
    {
        switch (align){
            case "left":{
                $('#text_area').css({'text-align':'left'});
                if(event){
                    $(".Alignment_wrapp").children().each(removeClassChoose);
                    $(event.target).parent().addClass('choosed active');
                };

                setActiveElementAlign("left");
                break;
            }
            case "center": {
                $('#text_area').css({'text-align':'center'});
                if(event){
                    $(".Alignment_wrapp").children().each(removeClassChoose);
                    $(event.target).parent().addClass('choosed active');
                };

                setActiveElementAlign("center");
                break;
            }
            case "right": {
                $('#text_area').css({'text-align':'right'});
                if(event){
                    $(".Alignment_wrapp").children().each(removeClassChoose);
                    $(event.target).parent().addClass('choosed active');
                    setActiveElementAlign("right");
                };
                break;
            }
        }
        $(element).val(text);
    }

    function removeClassChoose(index, element)
    {
        if($(element).hasClass("choosed"))
            $(element).removeClass("choosed active");

    }

    function getAlignment()
    {
        var align = "left";
        $(".Alignment_wrapp").children().each(function(index, element){
            if($(element).hasClass("choosed") && $(element).attr("data-align") != "left")
                align = $(element).attr("data-align");
        });
        return align;
    }

    function setActiveElementAlign(align)
    {
        //var canvas = document.getElementById('product_designer_canvas').fabric;
        var object = canvas.getActiveObject();
        if(object instanceof  fabric.Text){
            object.textAlign = align;
            canvas.renderAll();
        }
    }

    $("#back_color, #text_color").click(function(event){
        if(!(this.id == 'back_color' && $("#transparent_check").prop("checked"))){
            $(this).children('input').focus();
        }
    });

    $("#back_color_input, #text_color_input").change(function(){
        if(!(this.id == 'back_color_input' && $("#transparent_check").prop("checked"))){
            var color = '#' + $(this).val();
            var elem = $(this).parent().children('div');
            elem.css('background-color',color);
        }
    });
    $("#transparent_check").change(function(){
        var object = canvas.getActiveObject();
        if($(this).prop("checked")){
            //var canvas = document.getElementById('product_designer_canvas').fabric;
            if (object != undefined && object instanceof  fabric.Text){
                delete object.textBackgroundColor;
                object.textBackgroundColor = false;
                canvas.renderAll();
            }
            document.getElementById('back_color_input').color.fromString('#FFFFFF');
            $("#back_color > div").css('background-color', '#FFFFFF');
            $("#back_color").css("display","none");
            $("#back_color_label").css("display","none");
        } else {
            var backgroundColor = "#"+$("#back_color_input").val();
            if (object != undefined && object instanceof  fabric.Text){
                object.setTextBackgroundColor(backgroundColor);
                canvas.renderAll();
            }
            $("#back_color").css("display","block");
            $("#back_color_label").css("display","block");
        }
    });

    $('#text_area').bind('input propertychange', function() {
        //var canvas = document.getElementById('product_designer_canvas').fabric;
        var object = canvas.getActiveObject();
        if(object instanceof  fabric.Object && object instanceof  fabric.Text){
            var strText = $("#text_area").val();

            var align = getAlignment();
            object.setText(strText);
            object.textAlign = align;
            canvas.renderAll();
            canvas.fire('object:modified');
        } else {
            canvas.fire('object:modified');
            addText();
        }
    });

    //block for color effects apply to active object (for only image)
    $('.effects_list img').click(function(){
        posit = $(this).attr('sel-pos');
        $('.effect_selector span').fadeOut();
        var effectId = '#' + $(this).attr('ef-id');
        applyEffectToAObject(effectId);
        $(effectId).fadeIn();
        $('.effect_selector').animate({
            'left' : posit
        },100);
    });

    function applyEffectToAObject(effectId){
        var elemId = effectId;
        //var canvas = document.getElementById('product_designer_canvas').fabric;
        var object = canvas.getActiveObject();

        if (typeof object != 'undefined' ){
            if (object instanceof fabric.Image){
                if(typeof object.grayscale == 'undefined')
                    object.grayscale = false;
                if(typeof object.sepia == 'undefined')
                    object.sepia = false;
                if(elemId == "#original_effect"){
                    object.filters.pop();
                    object.filters.pop();
                    object.filters.pop();
                    object.grayscale = false;
                    object.sepia = false;
                    object.applyFilters(canvas.renderAll.bind(canvas));
                }else if(elemId =="#grayscale_effect"  && !object.grayscale ){
                    object.filters.push(new fabric.Image.filters.Grayscale());
                    object.applyFilters(canvas.renderAll.bind(canvas));
                    object.grayscale = true;
                    object.sepia = false;
                }else if(elemId =="#sepia_effect"  && !object.sepia){
                    object.filters.push(new fabric.Image.filters.Sepia3());

                    object.applyFilters(canvas.renderAll.bind(canvas));
                    object.sepia = true;
                    object.grayscale = false;
                }
            }
        }
    }

    $(".style_btn").on('click', function() {
        var textColor = $(this).data('text');
        var backgroundImage = $(this).data('image');

        /*topText.fill = textColor;
        bottomText.fill = textColor;
        rectTopText.backImage = false;
        rectBottomText.backImage = false;*/
        if (backgroundImage != undefined){
            var imgBackground = fabric.Image.fromURL(backgroundImage, function(oImg) {
                var height = oImg.getHeight(),
                    width = oImg.getWidth();
                var coeff = 1;
                if (width > 600){
                    coeff = 1/ (width / 600);
                }
                scale = coeff;
                oImg.scale(scale);
                canvas.setBackgroundImage(oImg,canvas.renderAll.bind(canvas));
            });
        }
        canvas.forEachObject(function(obj){
            if (obj instanceof fabric.Text){
                obj.fill = textColor;
                canvas.renderAll();
            }
        });
        canvas.renderAll();
        var activeElement = $('.widget_body').find('.active');
        if (activeElement != undefined){
            activeElement.removeClass('active');
        }
        $(this).addClass('active');
    });

    function setScale(scale)
    {
        if(scale > 2 || scale < 0.5)
            return;
        var left = scale * 50;
        $(".point").css({"left": left+ 'px'});
    }
    //listener for deleting active object at fabric if del key was pressed
    $(document).keydown(function(e){
       if(e.keyCode == 46 && !canvas.framedesign)
            fabricActiveObjectOperation('remove');
    });

    $(document).ready(function(){
        if($("#thisCustomizeType").length && $("#thisCustomizeType").data("customize-type") == 'engraved' && $("#thisCustomizeType").data("type")=="plate"){
            $('span:contains("Silver/Black")').parent().trigger("click");
            //console.log("Silver/Black");
        }

    });

    var element =  $('.font_selector').find('[data-font="New_Arial_Black"]');
    if (element != undefined){
        element.trigger("click");
    }
    $('#init').on('click', function() {
        $('select[name="colorpicker-picker-shortlist"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('select[name="colorpicker-picker-longlist"]').simplecolorpicker({picker: true, theme: 'glyphicons'})
            .on('change', function(){
               var color = $('select[name="colorpicker-picker-longlist"]').val();
               var el = canvas.getActiveObject();
               if (el != undefined &&  el instanceof fabric.Text){
                   stackForStates.push();
                   el.setFill(color);
                   canvas.renderAll();
               }
        });
        $('select[name="colorpicker-picker-longlist-back"]').simplecolorpicker({picker: true, theme: 'glyphicons'})
            .on('change', function(){
                stackForStates.push();
                stackForStates.push();
                var color = $('select[name="colorpicker-picker-longlist-back"]').val();
                canvas.setBackgroundColor(color,canvas.renderAll.bind(canvas));
                canvas.backgroundImage = null;
                canvas.backgroundColorChange = true;
                canvas.renderAll();
            });
        $('select[name="colorpicker-picker-delay"]').simplecolorpicker({picker: true, theme: 'glyphicons', pickerDelay: 1000});
        $('select[name="colorpicker-picker-option-selected"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('select[name="colorpicker-picker-options-disabled"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('select[name="colorpicker-picker-option-selected-disabled"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('select[name="colorpicker-picker-optgroups"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
    });
    $('#destroy').on('click', function() {
        $('select').simplecolorpicker('destroy');
    });
    // By default, activate simplecolorpicker plugin on HTML selects
    $('#init').trigger('click');

});






































