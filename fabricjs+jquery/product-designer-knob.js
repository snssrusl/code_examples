/**
 * Created by user on 2/5/14.
 */
$(function(){
    var canvas = document.getElementById('product_designer_canvas').fabric;
    $(".knob").knob({
        change : function (value) {
        },
        release : function (value) {
        },
        cancel : function () {
        },
        draw : function () {
            // "tron" case
            if(this.$.data('skin') == 'tron') {
                var a = this.angle(this.cv); // Angle

                var angleInDegrees = this.angle(this.cv)*180/Math.PI;
                angleInDegrees = (angleInDegrees >= 40 && angleInDegrees <=50)? 45: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 85 && angleInDegrees <=95)? 90: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 130 && angleInDegrees <=140)? 135: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 175 && angleInDegrees <=185)? 180: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 220 && angleInDegrees <=230)? 225: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 265 && angleInDegrees <=275)? 270: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 310 && angleInDegrees <=320)? 315: angleInDegrees;
                angleInDegrees = (angleInDegrees >= 355 && angleInDegrees <=360 || angleInDegrees >=0 && angleInDegrees <=5)? 0: angleInDegrees;
                //console.log(angleInDegrees);
                a = (angleInDegrees*Math.PI)/180;

                var sa = this.startAngle          // Previous start angle
                    , sat = this.startAngle         // Start angle
                    , ea                            // Previous end angle
                    , eat = sat + a                 // End angle
                    , r = 1;

                this.g.lineWidth = this.lineWidth;

               this.o.cursor
                    && (sat = eat - 0.3)
                && (eat = eat + 0.3);
                var coefff = 1;
                if (window.devicePixelRatio == 2){
                    this.g.scale(2,2);
                    //this.radius = this.radius*0.9;
                    coefff = 0.45;
                }
                this.g.lineWidth = 4;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                var ang = sat+0.3;
                var tmp_x = coefff*(this.radius-15)*Math.cos(ang),
                    tmp_y = coefff*(this.radius-15)*Math.sin(ang);

                var tmp_x1 = coefff*(this.radius-22)*Math.cos(ang-0.12),
                    tmp_y1 = coefff*(this.radius-22)*Math.sin(ang-0.12);
                var tmp_x2 = coefff*(this.radius-22)*Math.cos(ang+0.12),
                    tmp_y2 = coefff*(this.radius-22)*Math.sin(ang+0.12);

                var coord_x = tmp_x+80,
                    coord_y = tmp_y+80;
                var coord_x1 = tmp_x1+80,
                    coord_y1 = tmp_y1+80;
                var coord_x2 = tmp_x2+80,
                    coord_y2 = tmp_y2+80;


                var centerX = coord_x;
                var centerY = coord_y+2;
                var radius = 7;
                this.g.beginPath();
                this.g.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
                this.g.fillStyle = '#888';
                this.g.fill();
                this.g.lineWidth = 2;
                this.g.strokeStyle = '#3a3637';
                this.g.stroke();
                var object = canvas.getActiveObject();
                if(typeof object != 'undefined' && object != null){
                    object.rotate(angleInDegrees);
                    canvas.renderAll();
                    if(canvas.rotateControl != undefined && !canvas.rotateControl){
                        canvas.fire("object:modified");

                    }
                    canvas.rotateControl = false;
                }
                /*this.g.moveTo(coord_x,coord_y);
                this.g.lineTo(coord_x1,coord_y1);
                this.g.closePath();
                this.g.stroke();
                this.g.beginPath();
                this.g.moveTo(coord_x,coord_y);
                this.g.lineTo(coord_x2,coord_y2);
                this.g.closePath();
                this.g.stroke();*/

                this.g.lineWidth = 4;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                // this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
            }
        }

    });

    //$(".knob").val(1).trigger("change");
    $("#reset_controls").trigger("click");
    //$(".knob").trigger("mouseover").trigger("click");


});





