var mraid = window.parent.mraid;
if ("undefined" == typeof mraid) {
    var bridge = {
        receiver: window.parent,
        send: function(e, i) {
            return bridge.receiver.controller.postMessage({
                target: "controller",
                action: e,
                arguments: i
            }, "*")
        }
    };
    mraid = {
        _version: "2.0",
        _state: "loading",
        _placementType: "inline",
        _defaultPosition: {
            x: 0,
            y: 0,
            width: -1,
            height: -1
        },
        _customClose: !1,
        _expandProperties: {
            useCustomClose: !1,
            width: "auto",
            height: "auto",
            isModal: !0,
            lockOrientation: !1
        },
        _resizeProperties: {
            width: -1,
            height: -1,
            offsetX: -1,
            offsetY: -1,
            customClosePosition: "top-right",
            allowOffscreen: !0
        },
        _listeners: {
            ready: [],
            error: [],
            stateChange: [],
            viewableChange: [],
            sizeChange: []
        },
        _orientationProperties: {
            allowOrientationChange: false,
            forceOrientation: "none"
        },
        _container: 'container',
        _wExp: undefined,
        _event: function(e) {
            var i = Array.prototype.slice.call(arguments, 1, arguments.length);
            for (var t in mraid._listeners[e]) mraid._listeners[e][t].apply(mraid, i)
        },
        _loaded: function() {
            mraid._viewable = !0, mraid._stateChangeEvent("default"), mraid._event("ready", "ready"),
                function e(i) {
                    var t = mraid.isViewable();
                    i != t && mraid._event("viewableChange", t), setTimeout(function() {
                        e(t)
                    }, 100)
                }(mraid.getCurrentPosition(mraid.isViewable()))
        },
        _orientation: function() {
            //return 90 == Math.abs(bridge.receiver.orientation) ? "landscape" : "portrait"
            return 'orientation'
        },
        _stateChangeEvent: function(e) {
            mraid._state = e, mraid._event("stateChange", e)
        },
        _setToDefaultValue: function(){
            mraid._setProperties("_expandProperties", ["height", "width"], {height: "auto", width: "auto" });
            mraid._setProperties("_resizeProperties", ["width", "height", "offsetX", "offsetY"], {width: -1, height: -1, offsetX: -1, offsetY: -1})
        },
        _errorEvent: function(e, i) {
            mraid._event("error", e, i)
        },
        _validators: {
            dimension: function(e) {
                return !isNaN(e) && e >= 0
            },
            type: function(e, i) {
                return typeof e === i
            },
            inclusion: function(e, i) {
                return i.indexOf(e) > -1
            }
        },
        _getContainer: function() {
            if (mraid._container.parentNode != undefined && mraid._container.parentNode != null) {

            } else {
                var el = document.getElementsByTagName('adquota-tag')[0];
                return el;
            }
        },
        _validate: function(e, i, t) {
            return mraid._validators[i].call(!1, e, t) ? !0 : (mraid._errorEvent(i + " validation fails for value: " + e + " and options: " + t, "_validate"), !1)
        },
        _validateExpandProperties: function(e) {
            return mraid._validate(e.width, "dimension") && mraid._validate(e.height, "dimension") && mraid._validate(e.useCustomClose, "type", "boolean")
        },
        _validateResizeProperties: function(e) {
            return mraid._validate(e.width, "dimension") && mraid._validate(e.height, "dimension") && mraid._validate(e.offsetX, "dimension") && mraid._validate(e.offsetY, "dimension") && mraid._validate(e.customClosePosition, "inclusion", ["top-left", "top-center", "top-right", "center", "bottom-left", "bottom-center", "bottom-right"])
        },
        _pxToDip: function(e) {
            return e
        },
        addEventListener: function(e, i) {
            mraid._listeners[e] ? mraid._listeners[e].push(i) : mraid._errorEvent("can not add event to unexisting kind " + e, "addEventListener")
        },
        createCalendarEvent: function() {
            mraid._errorEvent("not supported", "createCalendarEvent")
        },
        close: function() {
            "default" == mraid.getState() ? (mraid._event("sizeChange", 0, 0), mraid._stateChangeEvent("hidden"), hideContainer()) : ("expanded" == mraid.getState() || "resized" == mraid.getState()) && (mraid._event("sizeChange", mraid.getDefaultPosition().width, mraid.getDefaultPosition().height), mraid._stateChangeEvent("default"), resizeToDefault(), mraid._setToDefaultValue());
            function resizeToDefault(){
                var el = mraid._getContainer();
                el.parentNode.setAttribute("style", "width: " + mraid._defaultPosition.width + "px; height: " + mraid._defaultPosition.height+"px;");
                var img = el.parentNode.getElementsByTagName("img")[0];
                img.setAttribute("style", "width: " + mraid._defaultPosition.width + "px; height: " + mraid._defaultPosition.height+"px;");
                if (mraid._wExp != undefined){
                    mraid._wExp.close(), mraid._wExp = undefined;
                }
            }
            function hideContainer(){
                var el = mraid._getContainer();
                //console.log("Hide container");
                el.parentNode.setAttribute("style", "display: none;");
            }
        },
        expand: function(e) {
            return "resized" != mraid.getState() && "default" != mraid.getState() ? (/*mraid._errorEvent("Ad can be expanded only in default or resized state", "expand"), !1*/ console.log("Ad can be expanded only in default or resized state"))
                : e != undefined ? function(ei){
                    var el = mraid._getContainer();
                    var styleParent = "display: block; position:fixed;top:0px; left:0px;padding:5px;background-color:#fff;width:100%;height:100%;border:1px solid black; z-index:10000;";
                    var newContainer = document.createElement('div');
                    newContainer.setAttribute("style", styleParent);
                    if ((ei).indexOf('.js') !=-1){
                        var jsLinkTag = document.createElement('script');
                        jsLinkTag.setAttribute('type','text/javascript');
                        jsLinkTag.setAttribute('src', ei);
                        newContainer.appendChild(jsLinkTag);

                    } else {
                        var req = new XMLHttpRequest();
                        req.open(
                            "GET",
                            ei,
                            true);
                        req.onreadystatechange = statusListener;
                        req.send(null);

                        function statusListener()
                        {
                            if (req.readyState == 4)
                            {
                                if (req.status == 200) {
                                    var site = req.responseXML;
                                    newContainer.appendChild(site);
                                }
                            }
                        }
                    }
                    el.parentNode.appendChild(newContainer);
                    }(e) : (mraid._event("sizeChange", mraid._expandProperties.width, mraid._expandProperties.height),
                function(e){
                    var el = mraid._getContainer();
                    if(mraid._expandProperties.width === "auto" && mraid._expandProperties.height === "auto") {
                        mraid._expandProperties.width = window.innerWidth;
                        mraid._expandProperties.height = window.innerHeight;
                    }
                    var styleParent = "display: block; position:fixed;top:0px; left:0px;padding:5px;background-color:#fff;width:100%;height:100%;border:1px solid black; z-index:10000;"
                    el.parentNode.setAttribute("style",styleParent);
                    var img = el.parentNode.getElementsByTagName("img")[0];
                    var imgUrl = img.src;
                    //console.log("img : " + imgUrl);

                    //console.log('Set expand: ' + el.parentNode);
            }(mraid._container), void mraid._stateChangeEvent("expanded"))
        },
        getCurrentPosition: function() {

            if (mraid._state === 'default'){
                return mraid._defaultPosition;
            }
            if (mraid._state === 'expanded'){
                var coord = {x: parseInt(mraid._defaultPosition.x),
                             y: parseInt(mraid._defaultPosition.y),
                             height: mraid._expandProperties.height,
                             width: mraid._expandProperties.width
                };
                return coord;
            }
            if (mraid._state === 'resized'){
                var container = mraid._getContainer();
                var mX = parseInt(container.parentNode.style['margin-left']),
                    mY = parseInt(container.parentNode.style['margin-top']);
                var coord = {x: mX,
                    y: mY,
                    height: mraid._resizeProperties.height,
                    width: mraid._resizeProperties.width
                };
                return coord;
            }
            if (mraid._state === "hidden"){
                var coord = {x: 0, y: 0, height: 0, width: 0};
                return coord;
            }
        },
        getDefaultPosition: function() {
            return mraid._pxToDip(mraid._defaultPosition)
        },
        getExpandProperties: function() {
            if (mraid._expandProperties.height === 'auto')
                mraid._expandProperties.height = screen.height;
            if (mraid._expandProperties.width === 'auto')
                mraid._expandProperties.width = screen.width;
            return mraid._expandProperties
        },
        getMaxSize: function() {
            return mraid._pxToDip({
                width: window.innerWidth,
                height: window.innerHeight
            });
        },
        getPlacementType: function() {
            return mraid._placementType;
        },
        getResizeProperties: function() {
            return mraid._resizeProperties
        },
        getScreenSize: function() {
            return mraid._pxToDip("portrait" == mraid._orientation() ? {
                width: screen.width,
                height: screen.height
            } : {
                width: screen.height,
                height: screen.width
            })
        },
        getState: function() {
            return mraid._state
        },
        getOrientationProperties: function(){
            return mraid._orientationProperties;
        },
        getVersion: function() {
            return mraid._version
        },
        isViewable: function() {
            var e = mraid.getCurrentPosition();
            return e.x + e.width > 0 && e.y + e.height > 0
        },
        open: function(e) {
            window.open(e, "")
        },
        playVideo: function() {
            mraid._errorEvent("not supported", "playVideo")
        },
        removeEventListener: function(e, i) {
            var t = !1;
            if (mraid._listeners[e])
                for (var r = 0; r < mraid._listeners[e].length; r++) mraid._listeners[e][r] == i && (t = !0, mraid._listeners[e].splice(r, 1));
            t || mraid._errorEvent("no event listener was removed", "removeEventListener")
        },
        resize: function() {
            return "resized" != mraid.getState() && "default" != mraid.getState() ? (mraid._errorEvent("Ad can be resized only in default or resized state", "resize"), !1) :
                   /* 1 != mraid._validateResizeProperties(mraid._resizeProperties) ? (mraid._errorEvent("Ad can be resized only when valid resized properties are set", "resize"), !1) : */(mraid._event("sizeChange", mraid._resizeProperties.width, mraid._resizeProperties.height),
                function(e){

                var el = mraid._getContainer();

                //console.log("start resize");

                el.parentNode.setAttribute("style", "width: " + mraid._resizeProperties.width + "px; height: " + mraid._resizeProperties.height+"px; margin-top: "
                    + (parseInt(mraid.getCurrentPosition().y)+mraid._resizeProperties.offsetY) + "px; margin-left: " + (parseInt(mraid.getCurrentPosition().x)+mraid._resizeProperties.offsetX) +"px;");
                var img = el.parentNode.getElementsByTagName("img")[0];
                //console.log("img : " + img);
                //console.log('Set resize: ' + el.parentNode);
            }(mraid._container), void mraid._stateChangeEvent("resized"))
        },
        _setProperties: function(e, i, t) {
            for (var r = i.length, n = 0; r > n; n++) {
                var a = i[n];
                t.hasOwnProperty(a) && (mraid[e][a] = t[a])
            }
        },
        setExpandProperties: function(e) {
            mraid._setProperties("_expandProperties", ["width", "height", "useCustomClose"], e)
        },
        setResizeProperties: function(e) {
            mraid._setProperties("_resizeProperties", ["width", "height", "offsetX", "offsetY", "customClosePosition", "allowOffscreen"], e)
        },
        setDefaultPosition: function(e) {
            mraid._defaultPosition = e;
        },
        setContainerElement: function(e){
            //console.log('Container: ' + e);
            mraid._container = e;
        },
        setOrientationProperties: function(e){
            mraid._setProperties("_orientationProperties", ["allowOrientationChange", "forceOrientation"], e)
        },
        storePicture: function() {
            mraid._errorEvent("not supported", "storePicture")
        },
        supports: function(e) {
            return {
                sms: !0,
                tel: !0,
                calendar: !1,
                storePicture: !1,
                inlineVideo: !1
            }[e]
        },
        useCustomClose: function(e) {
            mraid._expandProperties.useCustomClose = e
        }
    }, document.addEventListener("DOMContentLoaded", function() {
        mraid._loaded()
    }, !1), window.parent.mraid = mraid
   /* if(document.readyState === "complete") {
        mraid._loaded();
    }*/
}