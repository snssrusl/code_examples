/**
 * Created by ron on 8/11/14.
 */

(function(){


    //var document = window.document;

    var adQuotaTag = document.registerElement('adquota-Tag');

    var publisherRequestArray = ("zone-id,channel-category,channel-id,response-debug,ad-mode,device-ip,"+
        "device-ip6,device-ids-idfa,device-ids-openudid,device-ids-idx2,user-gender,"+
        "user-age,user-agerange,user-city,user-zipcode,user-country,user-incomplete,"+
        "user-income,user-incomecurrency,placement-width-min,placement-width-max,placement-height-min,"+
        "placement-height-max,placement-richmedia,placement-floorprice,placement-currency," +
        "custom-tags,custom-httpheaders,custom-click-redirect,custom-click-tracker," +
        "custom-impression-tracker,versions-app").split(',');
    var server = "http://api.int.adquota.com/rest/ads";
    var gTag = undefined;
    var positionTag = {
        x: 0,
        y: 0,
        width: -1,
        height: -1
    };
    var adquotaRequest = {
        jsonObject:{

        },
        init: function(){
            this.jsonObject['device'] = Object.create(Object.prototype);
            this.jsonObject.device.useragent = navigator.userAgent;
        },
        add: function(element,elValue){
            var arrElement = element.split('-');
            //console.log(arrElement);
            var length = arrElement.length
            if (length > 0){
                var el = this.jsonObject[arrElement[0]];
                if(!el){
                    this.jsonObject[arrElement[0]] = Object.create(Object.prototype);
                    el = this.jsonObject[arrElement[0]];
                }
                //console.log(el);
                for(var i = 1; i < length; ++i){
                    if(i === (length-1)){
                        if (elValue){
                            el[arrElement[length-1]] = elValue;
                        }
                        break;
                    }
                    //console.log(this.jsonObject);
                    var prevEl = el;
                    el = el[arrElement[i]];
                    if(!el && i !== (length-1)){
                        prevEl[arrElement[i]] = Object.create(Object.prototype);
                        el = prevEl[arrElement[i]];
                    }

                }
            }
        },
        toJson: function(){
            return JSON.stringify(this.jsonObject);
        }
    };
    window.onload = function(){

        var el = document.getElementsByTagName('adquota-Tag');
        var el = document.getElementsByTagName('adquota-Tag');
        var mraidScript = document.createElement('script');
        mraidScript.setAttribute('type','text/javascript');
        mraidScript.setAttribute('src', 'mraid.js');
        el[0].parentNode.appendChild(mraidScript);
        if (el && el.length > 0) {
            //console.log(el[0].attributes);
            adquotaRequest.init();
            var attributes = el[0].attributes;
            var length = attributes.length;
            for (var i = 0; i < length; ++i) {
                if (publisherRequestArray.indexOf(attributes[i].name) !== -1) {
                    adquotaRequest.add(attributes[i].name, attributes[i].value);
                }
            }
        }
        var jsonTag = adquotaRequest.toJson();
        xdr(server, 'POST', jsonTag, resultRequestOutput,resultErrorOutput);
    }

    function resultRequestOutput(response){
        var el = document.getElementsByTagName('adquota-tag')[0];

        //console.log("Server response: " + response);
        var responseObj = JSON.parse(response);
        var innerHtml = el.parentNode.innerHTML;
        innerHtml += responseObj.adm;
        //console.log("Inner " + innerHtml)
        gTag = responseObj.adm;
        positionTag.height = responseObj.size.h;
        positionTag.width = responseObj.size.w;

        if (mraid.getState() === 'loading'){
            //console.log("MRAID is loading");
            showAd();

        } else if (mraid.getState() != 'ready') {
            //console.log("MRAID Ad: adding event listener for ready");
            mraid.addEventListener('ready', showAd);
        } else {
            showAd();
        }
    }

    function resultErrorOutput(error){
        var el = document.getElementsByTagName('adquota-tag')[0];
        var innerHtml = el.parentNode.innerHTML;
        innerHtml += error;
        el.innerHTML = innerHtml;
    }

    function showAd() {
        var el = document.getElementsByTagName('adquota-tag')[0];

        //console.log('showAd function');
        var innerHtml = el.parentNode.innerHTML;
        if (gTag !== undefined) {
            if (positionTag.width > 0 && positionTag.height > 0) {
                //console.log('Set node');
                el.parentNode.style['width'] = positionTag.width+'px';
                el.parentNode.style['height'] = positionTag.height+'px';
            }
            innerHtml += gTag;
            el.parentNode.innerHTML = innerHtml;
            positionTag.x = el.offsetLeft;
            positionTag.y = el.offsetTop;
            mraid.setContainerElement(el);
            mraid.setDefaultPosition(positionTag);
            mraid._loaded();
        }
        gTag = undefined;

    }

    /**
     * Make a X-Domain request to url and callback.
     *
     * @param url {String}
     * @param method {String} HTTP verb ('GET', 'POST', 'DELETE', etc.)
     * @param data {String} request body
     * @param callback {Function} to callback on completion
     * @param errback {Function} to callback on error
     */
    function xdr(url, method, data, callback, errback) {
        var req;


        if(XMLHttpRequest) {
            req = new XMLHttpRequest();

            if('withCredentials' in req) {
                req.open('POST', url,true);
                req.setRequestHeader('Content-Type', 'application/json');
                req.onerror = errback;
                req.onreadystatechange = function() {
                    if (req.readyState === 4) {
                        if (req.status >= 200 && req.status < 400) {
                            callback(req.responseText);
                        } else {
                            errback(new Error('Response returned with non-OK status'));
                        }
                    }
                };
                req.send(data);
            }
        } else if(XDomainRequest) {
            req = new XDomainRequest();
            req.open('POST', url);
            req.onerror = errback;
            req.onload = function() {
                callback(req.responseText);
            };
            req.send(data);
        } else {
            errback(new Error('CORS not supported'));
        }
    }

})()

